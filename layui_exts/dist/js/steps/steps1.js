
("use strict");
layui.define(function(exports){
  let vm;

  let handler = {
    run: function(layero){
      
      let param = {
        el: layero.find('.layui-layer-content').get(0),
        data: {},
        mounted(){
          layui.use(['code', 'steps'],function(){
            layui.code();

            let html = '<ul class="ws-dir-ul" style="max-height: 530px;">'; 
            let selectors = layero.find('.layui-layer-content').get(0).querySelectorAll('.layui-elem-quote');
            let index = 0;
            Array.prototype.forEach.call(selectors, function(selector){
              let id = selector.getAttribute('id');
              let name = selector.getAttribute('title');
              if(id && name){
                html += `<li level="2" li-index="${index}" class=""><a href="#${id}">${name}</a></li>`;
                index++;
              }
            }); 
            html += '</ul>';
            layero.find('.layui-layer-content').append($(html));
            layero.find('.layui-layer-content').find('.ws-dir-ul').css('height', index * 30 + 'px');

            layero.find('.layui-layer-content').on('click', '.ws-dir-ul li', function(){
              layero.find('.layui-layer-content').find('.ws-dir-ul li.layui-this').removeClass('layui-this');
              $(this).addClass('layui-this');
            });

            setTimeout(function(){
              // 减小弹出层动画引起的容器宽度不准的问题,在这里重新调用,防止页面重新打开时不渲染
              layui.steps.buildAll();
            }, 500);

          })
        }
      }
      // 获取实例
      vm = layui.binder(param);
    },

    destroy: function(){
      vm && vm.$destroy && vm.$destroy();
    },

  };
  exports("steps1", handler);
});