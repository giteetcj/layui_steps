/**
 * 弹出层管理组件  V4.2.0
 *  @since 2023/01/06
 *
 * 测试版本 0
 * 今天主要是测试对部分触发函数进行防抖处理
 * 为了使动画可以流畅的触发
 *
 * 后面的测试版本
 * 菜单构想
 * 首先是从弹层配置项中提取信息
 * 里面有一个mark属性来标记已啥子字母开头
 * 要有一个pid来声明挂到那个下面，下面有子菜单的就不能点击开了
 * 这种文件夹应该是初始化的时候就一定要声明好。
 *
 * 磁贴构想
 * 首先是一个二维数组
 * 每次刷新磁贴之后后需要对整体的布局进行重新的计算
 * 使用absolute的布局，可以尝试后面的允许拖拽
 *
 *  @since 2023/01/10
 *
 * 今天是要处理一下小窗口的动画
 *
 * 当第一次触发显示的时候，不加动画，是直接调出来的。
 * 动画在大和小之间的切换也要自然
 * 首先是在关闭的时候清空这个hoverPool
 *
 * @since 2023/01/12
 * 1. 调整了一下操作栏上面的样式，添加上背景动画
 * 接下来是需要修改一下这个最小化和恢复的时候，动画起始位置要是接近li标签更好（这里也加了一个限制，最多偏移200个像素点，不然动画动作过大）
 * 在切换hover属性的时候要给li标签添加一个class属性，让被预览的它，鼠标指向它对应的小窗口时也能有hover样式
 *
 * @since 2023/01/15
 * 1. 接下来就是主菜单的构思了。
 *  1.1 首先要从配置项里面提取参数配置，如果没有参数配置的一律按照默认项处理
 *  1.2 新增配置项 mark 这个是作为 一级菜单 配置项的别名出现的，(一级菜单的特殊处理，如果匹配到它是一级菜单就不能直接打开页面了)
 *               以mark项作为标志，如果配置项里面有mark那么它是一级菜单的配置项，是不能打开页面的，其它页面的pi指向它的id
 *                它是只针对与主菜单存在的。mark建议是一个英文 or 拼音，以首字母作为特征字母。 如果不是就打入 # 了项里面
 *     新增配置项 prevent 这个是一级菜单里面配置了这一项可以阻止它进行open的方法，而是打开主菜单，展示这个一级菜单
 *     新增配置项 pid 这个是作为 指向它挂载在菜单里面的哪一个一级菜单下面的标志
 *
 *     如果这个项没有pid也没有mark属性，那么它就会直接的挂载在#下面
 *  1.3 首先在open方法里面修改，如果阻止打开就预留方法。
 *  1.4 确定主菜单配置
 *      首先是三级配置的原则，第一层是首字母和 #
 *      第二层是去扫描一级菜单配置项
 *       第三层是挂载剩余的菜单配置项
 *
 *
 *  1.5 在success成功回调里面需要将当前的配置项写入主菜单配置中
 *  1.6 icon增加一个配置项 iconColor 默认是  rgba(var(--windows-main-button-color), 1)
 * 将这个设置作为字符串的形式传入，可以增加页面的个性化设置
 *
 *
 *
 */
("use strict");
(function (root, factory) {
  if (typeof exports === "object") {
    module.exports = factory();
  } else if (typeof define === "function" && define.amd) {
    define([], factory());
  } else if (layui && layui.define) {
    // 情况一、在layui存在的情况下,直接使用layui加载这个模块
    layui.define(["layer", "binder"], (e) => e("windows", factory()));
  }
})(this, function () {
  /**
   * @global
   * @description
   *        窗口的jq对象
   */
  let $body = $("body");

  /**
   * 窗口管理视图实例对象
   */
  let VM = null;

  /**
   *
   * 在此处拦截open方法，不在layer.js里面操作了，减少影响
   * @since v2.5.1
   *
   * HISTORY:                                            *
   *     @author: Malphite                                 *
   *     @Date: 2022-12-02
   *     @desc  在v4.0.0添加对于full,min,restore方法的修改
   *   虽然配置项里面有对应的回调函数，但是单独调用这些方法的时候并不会触发
   *   现在将回调函数放出来直接放在方法里面执行
   *
   */
  const openForever = layui.layer.open;
  /**
   * 拦截 full、 min、 restore 方法
   * @since v4.0.0
   *
   * 在js代码中直接调用 layer.full layer.min layer.restore
   * 不能触发layer参数中提供出来的回调函数
   * 这里重写这三个方法，确保回调函数可以被触发
   */
  const fullForever = layui.layer.full;
  const minForever = layui.layer.min;
  const restoreForever = layui.layer.restore;

  const CHARACTER_MAP =
    "A、B、C、D、E、F、G、H、I、J、K、L、M、N、O、P、Q、R、S、T、U、V、W、X、Y、Z";

  /**
   * 分屏常量
   * @param {*} title div的title属性
   * @param {*} width 窗口宽度的百分比 1 代表100%
   * @param {*} height 窗口高度的百分比 1 代表100%
   * @param {*} left 0 向左对齐 1 向右对齐
   * @param {*} top 0 向上对齐 1 向下对齐
   * @param {*} key 系数  id
   */
  const DEFAULT_DIVIDE = {
    1: {
      title: "上",
      width: 1,
      height: 0.5,
      left: 0,
      top: 0,
      key: 1,
    },
    2: {
      title: "下",
      width: 1,
      height: 0.5,
      left: 0,
      top: 1,
      key: 2,
    },
    3: {
      title: "左",
      width: 0.5,
      height: 1,
      left: 0,
      top: 0,
      key: 3,
    },
    4: {
      title: "右",
      width: 0.5,
      height: 1,
      left: 1,
      top: 0,
      key: 4,
    },
    5: {
      title: "左上",
      width: 0.5,
      height: 0.5,
      left: 0,
      top: 0,
      key: 5,
    },
    6: {
      title: "右上",
      width: 0.5,
      height: 0.5,
      left: 1,
      top: 0,
      key: 6,
    },
    7: {
      title: "左下",
      width: 0.5,
      height: 0.5,
      left: 0,
      top: 1,
      key: 7,
    },
    8: {
      title: "右下",
      width: 0.5,
      height: 0.5,
      left: 1,
      top: 1,
      key: 8,
    },
  };
  /**
   * 将上面的常量转化成list
   */
  let DEFAULT_DIVIDE_LIST = [];
  _lodash.each(DEFAULT_DIVIDE, (v) => {
    DEFAULT_DIVIDE_LIST.push({
      title: v.title,
      key: v.key,
    });
  });

  /**
   * @global
   * @constant
   *
   * @description
   * 描述：
   *        配置参数合集,其中常量 constant 可以由外界配置
   * HISTORY:                                            *
   *     @author: Malphite                                 *
   *     @Date: 2022-11-28
   */
  let config = {
    /**
     * @member {Object} 配置参数常量
     * @description
     *  描述 ：
     *
     *          存放窗口设置过程中相对于比较静态的一些常量数值集
     *          {@linkplain windowsProxy.wakeUp  在layerManage唤醒的时候会根据传入的参数重新配置参数 }
     *          在上述方法中，传入的第一个参数就是用来替换该项的默认配置的
     *
     */
    constant: {
      /**
       * @var {String} 第三方登录用户名
       * @description
       *    取代下面的动态用户名
       */
      DYNAMIC_OWNER: "",

      /**
       * @var {Boolean} 装载动画
       */
      DYNAMIC_STARTER: false,

      /**
       * @var {String} 选项卡外层id
       * @description
       *
       *    模拟选项卡，这里使用了layui admin里面的选项卡样式，最外层的id是照搬的
       */
      HEADERS_ID: "LAY_app_tabsheader",

      /**
       * @var {Number} 当前最高层级弹层的id
       * @description
       *
       *    弹层最大层级zIndex的值
       */
      MAX_INDEX: 19891014,

      /**
       * @var {Number} 当前最高层级弹层的id
       * @description
       *    这个id指的是layui.layer.open里面返回的id
       */
      TOP_INDEX: 0,

      /**
       * @var {String} layui隐藏class
       */
      HIDE: "layui-hide",

      /**
       * @var {Number} 全局递增量
       */
      INTERNAL_INDEX: 0,

      /**
       * @var {String} 容器的id名称
       */
      CONTAINER_ID: "layui-windows-bottom",

      /**
       * @var {String}  layui模块自动加载后自动被执行的方法名称
       */
      METHOD_AUTOEXEC_NAME: "run",

      /**
       *  @var {String}  降低层级的类选择器
       *  @description
       *
       *  作用:
       *
       *    {@linkplain config.constant.MAX_CLASSNAME 见MAX_CLASSNAME}
       */
      MIN_CLASSNAME: "layui-min-header",

      /**
       * @var {String} 标记层级的类选择器
       * @description
       *
       *  作用:
       *
       *        防止因为弹层原因引起的弹层问题
       *      1.在页面初始化时需要为那些可能受影响的区域添加一个 MAX_CLASSNAME 的类选择器
       *      2.在弹层最大化的时候会给之前标记的div添加 MIN_CLASSNAME的类选择器，强行降低它们的层级
       *      3.在弹层恢复会关闭的时候去掉  MIN_CLASSNAME的类选择器。
       *
       *  相关：
       *
       *    {@linkplain config.constant.MIN_CLASSNAME 降低层级的类选择器}
       */
      MAX_CLASSNAME: "layui-max-header",

      /**
       *  @var {String}  模拟最小化的class类
       *  @description
       *
       *  作用:
       *
       *    {@linkplain config.constant.MAX_CLASSNAME 见MAX_CLASSNAME}
       */
      MIN_ACTION_CLASS: "layui-layer-min-action",

      /**
       *  @var {String}  模拟恢复的class类
       *  @description
       *
       *  作用:
       *
       *    {@linkplain config.constant.MAX_CLASSNAME 见MAX_CLASSNAME}
       */
      MAX_ACTION_CLASS: "layui-layer-max-action",

      /**
       * @var {String} 预览窗口组div的外层 类选择器
       */
      CONTAINER_TIPS_CLASSNAME: "layui-windows-tip",

      /**
       * @var {Number} 初始窗口弹出的top位置
       */
      BASE_WINDOWS_TOP_OFFSET: 50,

      /**
       * @var {Number} 窗口距离下方的最小距离
       */
      BASE_WINDOWS_BOTTOM_OFFSET: 85,

      /**
       * @var {Number} 窗口距离左方的最小距离
       */
      BASE_WINDOWS_LEFT_OFFSET: 10,

      /**
       * @var {Number} 窗口距离右方的最小距离
       */
      BASE_WINDOWS_RIGHT_OFFSET: 10,

      /**
       * @var {String}  窗口上下偏移方向
       * @description
       *
       *  值:
       *
       *        DOWN    向下
       *        UP      向上
       */
      BASE_WINDOWS_TOP_DIRECTION: "DOWN",

      /**
       * @var {String} 窗口左右偏移方向
       * @description
       *
       *  值:
       *
       *        LEFT    向左
       *        RIGHT   向右
       */
      BASE_WINDOWS_LEFT_DIRECTION: "RIGHT",

      /**
       * @var {Number} 窗口每次的偏移量
       */
      BASE_WINDOWS_OFFSET: 30,

      /**
       * @var {Number} 窗口默认宽度
       */
      BASE_WINDOWS_WIDTH: 380,

      /**
       * 在固定分组时，给弹出的分组小窗口加上的class
       */
      FIXED_LI_TAG_CLASSNAME: "select-tip-fixed",

      /**
       * 添加上这个class，让弹出的分组小窗口显示出来(靠近操作栏的位置)
       * 这个和下面的class区别是这个是操作栏上面的li标签触发
       */
      DISPLAY_LI_TAG_CLASSNAME: "select-tip",

      /**
       * 添加上这个class，让弹出的分组小窗口显示出来(靠近操作栏的位置)
       * 这个和上面的class区别是这个是小窗口触发
       */
      OTHER_DISPLAY_LI_TAG_CLASSNAME: "select-tip-flag",

      /**
       * 菜单点击的默认事件
       */
      MENU_CLICK_EVENT: function (id) {
        return layui.layer.open(id);
      },
    },
    /**
     * @member {Object} 响应式数据
     * @description
     *
     *  成员:
     *
     *            @prop {String} username           用户名称
     *            @prop {Array} livelyPool         活跃池
     *            @prop {Object} hover              选中窗口原始状态配置项
     *            @prop {Array} hoverPool          分组小窗口配置对象
     *            @prop {Object} flip               3D Flip(卡片式多任务窗口) 动态配置项
     *            @prop {Array} assign             合并窗口的分组对象
     *            @prop {Object} TEMP_ASSIGN_WINDOW 预备即将被合并的layer窗口描述对象
     *            @prop {String} menuVisible        卡贴目录是否可见
     *            @prop {Object} tipsIcon           卡贴临时描述对象
     *            @prop {Array} tipsSource         卡贴列表
     */
    base: {
      /**
       * @var {String} 用户名称
       * @description
       *
       *  作用：
       *
       *        1.在窗口卡贴区域会展示用户名称
       *        2.可能存在其它功能会用到用户的地方
       *
       *  来源:
       *
       *        1.在base配置项里面配置
       *        2.考虑从上述config.constant 里面传入
       *        3.由于config.constant可以从外界传入，那么这一项也可以从外界传入
       */
      username: "Malphite",
      /**
       * @var {Array} 活跃池(非常重要)
       * @description
       *
       *   成员：
       *
       *      @prop {Number}  index :  主键， 由系统自增自发生成,主管在li标签上面的排序
       *      @prop {String}  id    :  pageDescribe配置的key, 主管一切窗口动作。如果是沙箱窗口取临时配置项的 父级id，也就是原配置项的key。合并窗口就临时组装一个id。
       *      @prop {String}  name  ： 窗口名字，仅展示。同上
       *      @prop {Boolean}  select： 用于标记这个窗口分组是否被选中。  true 为选中
       *      @prop {String}  icon  ： 给这个窗口添加一个icon。填写layui的icon码
       *      @prop {String}  iconColor  ： v4.2.0 新增     为上面的icon添加一个控制颜色的变量 iconColor 默认值是 rgba(var(--windows-main-button-color), 1)
       *      @prop {Array}  children： 这个窗口分组下属所有子窗口的配置项key的array集合。在窗口预览是弹出的小窗口就是要根据它来生成的。
       *      @prop {Boolean} fixed： 标记这个分组是否被固定在操作栏 4.0.0 新增  true为固定  false为不固定
       */
      livelyPool: [],
      /**
       * @var {Object} 选中窗口原始状态配置项
       * @description
       *
       *    作用：
       *
       *        当鼠标移到窗口对应的小窗口上时，这个窗口被临时置顶。这个参数记录窗口在指定前的状态，方便状态回滚
       *        2.0.0  更新: 仅保留id(pageDescribe配置的key),其它的状态信息在 pageDescribe配置项里面做
       *
       *    成员：
       *
       *        @prop {String} id: pageDescribe配置的key 标志当前正在预览这个窗口  为空或者0表示当前无窗口正在预览
       */
      hover: { id: 0 },
      /**
       * @var {*} 待触发预览事件的窗口id
       * @description
       *
       *    希望在鼠标滑过预览小窗口的时后，预览事件触发不要太灵敏(页面防抖)
       *  在触发事件 {@linkplain windowsProxy.appendHover}
       *            {@linkplain windowsProxy.doAppendHover}
       *   上面添加了延时器。在鼠标触发时先将触发的窗口id记下来，但是并不马上触发。
       * 在过段时间后检查当前的窗口id和记录一致再触发事件。
       *   可以避免鼠标不小心划过预览窗口就马上触发预览动画造成的页面画面闪动
       */
      hoverWindowID: 0,
      /**
       * @var {*} 当前预览分组下面的子窗口数量
       * @description
       *
       *    记录下来,在下次展示的时候通过与窗口数量的比较确定css动画。
       *  这个值乘以下面的值可以得到上次的窗口总宽度。
       */
      hoverGroupSize: 0,
      /**
       * @var {*} 一个预览窗口的规定宽度
       */
      hoverGroupWidth: 200,
      /**
       * @var {Array} 分组小窗口配置对象
       * @description
       *
       *    来源:
       *
       *        鼠标放在li标签上的时候会根据这个li标签对应的 livelyPool值(主要是children) 来动态生成
       *
       *    成员：
       *
       *        @prop {String} id:  小窗口对应窗口的id(pageDescribe配置的key) 通过它来操作窗口
       *        @prop {String} name: 窗口名字，主要用作展示
       *        @prop {String} groupid： 当前分组id， livelyPool里面的id值，在监听事件里面用来定位li标签
       *
       *    监听事件:
       *
       *        watchs.hoverPool
       *        当这个值发生改变时，主要是通过小窗口上面的关闭按钮关闭页面。这个时候要根据现有条件再次适应剩下的布局
       */
      hoverPool: [],
      /**
       * @var {Object} 3D Flip(卡片式多任务窗口) 动态配置项
       * @description
       *
       *  成员:
       *
       *        @prop {Array}  list                  当前展示的窗口队列   v2.5.0 废弃
       *        @prop {Object}  map                  当前展示的窗口原始信息  v2.5.0 新增
       *        @prop {Boolean} show                 卡片式多任务窗口展示情况  true 显示
       *        @prop {Number} size                  最大展示窗口数量
       *        @prop {Number} now                   当前读取缓存池的下标
       */
      flip: {
        /**
         * @var {Array} 当前展示的窗口队列
         * @description
         *
         *    作用：
         *
         *        指导多任务窗口中正确的排布渲染出各个窗口
         *
         *    来源:
         *
         *        在调用 getFlipList 来根据当前的状态获取
         *
         *    成员：
         *
         *        @prop {String}  id: 对应窗口的id(pageDescribe配置的key) 通过它来操作窗口
         *        @prop {String}  name: 窗口名字，主要用作展示
         * @deprecated  v2.5.0 移除，接下来由下面的map进行描述即可
         */
        list: [],

        /**
         * @var {Object} 当前展示的窗口的原始信息
         * @since v2.5.0
         * @description
         *
         *    作用:
         *
         *        在窗口动画之前记录下窗口当前的状态以便后面的还原
         *
         *    来源:
         *
         *        在调用 getFlipList 来根据当前的状态获取
         *
         *    成员: (key 为  {@link config.pageDescribe} 的key或者id)
         *
         *         @prop {Number}  offsetTop      窗口距离上的值
         *         @prop {Number}  offsetLeft     窗口距离左的值
         *         @prop {Number}  currentWidth   窗口当前宽度
         *         @prop {Number}  currentHeight  窗口当前高度
         *         @prop {Number}  zIndex         窗口当前zIndex从pageDescribe里面有
         *         @prop {Number}  min            窗口当前是否最小化 从pageDescribe里面有
         *
         */
        map: {},

        /**
         * @prop {Boolean} 窗口展示情况
         * @description
         *
         *  值：
         *
         *        true   显示
         *        false  隐藏
         */
        show: false,

        /**
         * @prop {Number} 最大展示窗口数量
         * @description
         *
         *        1.为了满足html动态加载，防止窗口过多造成页面卡死
         *        2.现有css限制仅有有现个窗口可以交互
         */
        size: 6,

        /**
         * @prop {Number} 当前读取缓存池的下标
         */
        now: 0,
      },
      /**
       * @var {Object} 分屏 动态配置项
       * @description
       *
       *  成员:
       *
       *        @prop {Array}  source   动态渲染dom
       *        @prop {Number}  point   当前指向的类型
       *        @prop {Number}  ACTION  是否展示dom
       */
      divide: {
        /**
         * @var {Array} 动态渲染dom
         * @description
         *
         *    成员：
         *
         *        @prop {String}  title
         *        @prop {String}  icon
         *        @prop {String}  key
         */
        source: DEFAULT_DIVIDE_LIST,

        /**
         * @prop {Number} 当前指向的类型
         */
        point: 0,

        /**
         * @prop {Number} 是否展示dom  0不展示  1展示
         */
        ACTION: 0,
      },
      /**
       * @var {Array} 主菜单配置项
       * @description
       *    这个是一个三级的结构
       *        1. 是首字母，它下面有children属性存放一级菜单
       *        2. 是一级菜单，它下面有children属性存放二级菜单
       *        3. 二级菜单
       *    这个都是以自己的配置项id作为id的。字母就是 character + 字母的大写形式
       */
      menus: [],

      /**
       * @var {Boolean} 主菜单的显示与隐藏
       * @description
       *    当它为false时主菜单隐藏
       *    当它为true时主菜单显示
       */
      menusVisable: "false",

      /**
       * @var {Boolean} 首字母菜单转换
       * @description
       *    当它为false时切换成菜单列表
       *    当它为true时切换成首字母列表
       */
      menusTrigger: "false",
    },

    temp: {
      /**
       * @var {Object} 活动中的   窗口弹层描述对象
       * @description
       *
       *  成员：
       *
       *          @prop {String}   id            pageDescribe配置的key(沙箱窗口指的是沙箱配置项的key)
       *          @prop {String}   name          窗口名称
       *          @prop {Boolean}   select        true or false 是否被选中
       *          @prop {Long}  updatetime    最近一次活跃的时间，时间越近，排序越靠前
       *          @prop {Number}   offsetTop     当前窗口距离top的距离 px
       *          @prop {Number}   offsetleft     当前窗口距离left的距离 px
       *          @prop {Boolean}   min        true or false 是否最小化
       *          @prop {Boolean}   show    true or false 是否窗口可视化
       *
       *  扩展成员: (在开启特殊功能时使用)
       *
       *          @prop {Number}   nowIndex     窗口记录的zindex值
       *          @prop {Number}   nowWidth     窗口记录的宽度
       *          @prop {Number}   nowHeight    窗口记录的高度
       *
       */
      windowsMap: {},

      /**
       * 窗口resize触发函数合集
       */
      resizeFn: {},

      /**
       * @var {Object} 当前正在被处理的窗口对象描述
       * @description
       *
       *  成员：
       *
       *          @prop {String}   id            pageDescribe / windowsMap 配置的key(沙箱窗口指的是沙箱配置项的key)
       *          @prop {Object}   layero        窗口对象
       *
       *  记录当前选中的窗口对象，但是在修改状态时修改的是 windowsMap 里面对应的值
       */
      disposeWindow: {},

      /**
       * @var {*} 待触发hover事件的分组id
       * @description
       *
       *    希望在操作栏选项上面触发的hover事件时避免不必要的重复渲染
       *  在触发事件 {@linkplain windowsProxy.showWindowTip}
       *    上面记录下当前正在被检阅的分组id,当再次调用这个方法时检查它是否还是要检阅这个分组
       *    如果是就直接返回，避免重复渲染
       */
      hoverGroupID: 0,
    },

    /**
     * @member {Object} 窗口描述对象(非常重要)
     * @description
     *
     *    来源：
     *
     *        1.在这个配置项里面直接配置
     *        2.通过wakeUp参数传入
     *        3.通过layer.open方法自动生成
     *
     *    变量：
     *
     *        --------------       layui.layer.open  经典参数    --------------------
     *
     *          id          pageDescribe配置的key,open 参数项里面的id项，没有自动生成临时配置对象(临时对象不支持沙箱，卡贴等功能)
     *
     *          type        基本层类型：这里默认给1。3.加载层、4tips层。不支持
     *          title       标题：这里默认没得，可以通过name属性自动生成
     *          content     内容：默认''
     *          skin        主题：这里建议不要设置  默认会添加全局主题
     *          area        宽高：这里默认会通过position()方法自动生成
     *          offset      坐标： 这里默认会通过position()方法自动生成
     *          ......
     *          shade       遮罩：这里默认0，带遮罩层的弹层不支持
     *          shift/anim  动画：这里默认1
     *          fixed              这里默认false
     *          maxmin   最大最小化  这里默认是true
     *
     *      ---------------      新增自定义参数         ---------------------
     *
     *          name      v1.0.0 新增     窗口名称(如果没得title属性会 合并title <b> name </b>)
     *          porperty  v1.0.0 新增     窗口沙箱策略 singleton(默认) 不启用沙箱   property启用沙箱
     *          snapshoot v2.0.0 废弃     窗口快照地址
     *          moudle    v1.0.0 新增     使用layui模块名称，如果有就会在页面成功打开之后自动载入并执行里面的run方法
     *          assign    v4.0.0 废弃     标志该窗口是否参与窗口合并操作
     *          assignBy  v4.0.0 废弃     assign分组的id 默认是""。另外存在这个值说明已经被合并了，监测时会放过对这个配置项的监测
     *          icon      v1.0.0 新增     窗口小图标 默认是""。拥有小图标会在li标签上使用小图标取代文字，卡贴上也会展示小图标。
     *          iconColor v4.2.0 新增     为上面的icon添加一个控制颜色的变量 iconColor 默认值是 rgba(var(--windows-main-button-color), 1)
     *          ignore    v2.5.0 新增     忽略标志  如果值为true 会放弃装饰此参数
     *          mark      v4.2.0 新增     一级菜单的配置项别名，首字母作为特征字母；配置项阻止打开窗口，调用时是弹出主菜单，指向当前这个配置菜单的位置
     *          pid       v4.2.0 新增     该配置项指向的一级菜单的id
     *
     *      ---------------      非配置参数(无须配置的临时参数) ---------------
     *
     *          index    v1.0.0 新增      保存由layer.open方法的返回值
     *          parent   v1.0.0 新增      保存当前窗口的jq对象
     *          topid    v1.0.0 新增      沙箱配置项指向原配置项的key
     *          porperty v1.0.0 新增      表明这个是一个沙箱配置项
     *          temp     v1.1.0 新增      表明这个窗口仅为临时配置窗口
     *          min      v1.0.0 新增      默认是false，窗口是否处于最小化状态
     */
    pageDescribe: {},

    /**
     * @member {Object}  沙箱临时缓存关系表
     */
    pageMultiple: {},

    timer: {
      resizeTimer: 0,
      showTipTimer: 0,
      showHoverTimer: 0,
      removeHoverTimer: 0,
    },
  };

  /**
   * @namespace PROPERTY
   * @private
   * @desc
   *
   *    1.页面配置描述,一般来说是不能外界随便修改
   *    2.内部记录临时变量
   *
   * HISTORY:                                            *
   *     @author: Malphite                                 *
   *     @Date: 2022-11-28
   */
  let PROPERTY = {
    /**
     * @member {String} windows当前版本号
     * @desc
     *
     *    以windows组件为0.0.1计算
     */
    VERSION: "4.2.0",

    /**
     * @member {Boolean} 判断是否是第一次加载 true代表已经加载过了
     */
    FIRST_LOADING: false,

    /**
     * @member {String} layerManage容器的id，动态往body里面插入dom时作为id
     */
    MANAGE_CONTAINER_ID: "layui-layer-layerManage",

    /**
     * @member {Number} 窗口合并等移动校验时的灵敏度
     * @desc
     *
     *        当top 与指定 top相差的绝对值在这个范围内的触发事件
     */
    MIN_OFFSET_TOP: 50,

    /**
     * @member {Number} 窗口合并等移动校验时的灵敏度
     * @desc
     *
     *        当left 与指定 left相差的绝对值在这个范围内的触发事件
     */
    MIN_OFFSET_LEFT: 50,
  };

  /**
   * @namespace rollPage
   * @private
   * @desc  实现选项卡定位逻辑
   *
   * 这里的逻辑是完全照搬layui admin里面的
   */
  let rollPage = {
    /**
     * 将内容向右边移动一个可视化距离
     * root.outerWidth()  可视化距离
     * prefLeft 下一步还能藏多远的距离，如果是正数说明不太够了，将第一项 left=0 的都要抽出来。
     */
    left: function (root, index) {
      // 1.首先获取到 菜单条  它距离容器左侧的距离
      let tabsLeft = parseFloat(root.css("left"));
      /**
       * 2.判断这个距离tabsLeft的值(这个值只能是小于等于00)
       *  情况一、这个值是等于0的，说明菜单条的左侧已经已经不能再向右边移动了。直接返回，不做改变
       * (仅仅使用  !tabsLeft  可能是 ''  或者 null  如果是 == 0 也不行 '' == 0 也是true
       *  所以满足 !tabsLeft 和  <= 0 两种条件的就只有 数字 0 了)
       *  情况二、这个值小于0
       */
      if (!tabsLeft && tabsLeft <= 0) return;
      /**
       * 3.计算需要移动的距离
       *  到此 tabsLeft必然小于0 ， root.outerWidth()菜单可视宽度是大于0 的
       *  -(tabsLeft + root.outerWidth())    ==>  - -tabsLeft  - root.outerWidth();
       *  - -tabsLeft 是菜单条超过左侧的距离
       *  那么prefLeft的实际意义是  菜单条 向右移动一个 菜单可视宽度，此时  菜单条和容器左侧的距离
       *
       *
       *
       *  prefLeft：首先使用菜单可视宽度(root.outerWidth())加上tabsLeft,得到移动后，原来展示的信息可保留的最大距离
       *    ( 相当于可视距离减去移动被替换的距离，得到剩下可保留的原来的最大距离 )
       *    因为这个tabsLeft必然小于0，所以最后的结果必然小于 root.outerWidth()
       *    情况一、如果这个距离大于0等于0，你左边超出的部分，菜单可视宽度完全可以展示出来，说明只需要把左边超出的部分移动展示出来。
       *    情况二、如果这个距离小于0，说明你左边超出的部分，要想一次展示出来，整个菜单可视距离都利用上还不够，只能展示一部分。
       */
      let prefLeft = -(tabsLeft + root.outerWidth());
      if (prefLeft >= 0) return root.css("left", 0);
      /**
       * 现在假设 强行将菜单的left设置为了0，菜单的左侧就对齐了，那么右侧会超出来一大截，超出的距离就是 prefLeft的等值
       * 此时
       * 依次遍历所有的li标签  它们left值第一个是0 后面慢慢增大
       * 当left值增加到等于或者超过 ‘prefLeft的等值’ 时，此时如果这个点处在菜单可视化左侧的0点，可以认为这样就刚刚好向右移了一个可视化距离
       *       a                b
       * |__________________|_________|   如果想求a比比长多少，可以将两个线段重合起来比较
       *               a-b
       * |___________|______|
       */
      root.children("li").each(function (index, item) {
        let li = $(item),
          left = li.position().left;
        if (left >= prefLeft) {
          root.css("left", -left);
          return false;
        }
      });
    },
    /**
     * 将所选中的内容展示到菜单可视范围内
     */
    auto: function (root, index) {
      let tabsLeft = parseFloat(root.css("left"));
      // 获得被选中li标签
      let thisLi = root.find('[lay-id="' + index + '"]');
      if (!thisLi[0]) return;
      let thisLeft = thisLi.position().left;
      // tabsLeft 必然是一个负数  -tabsLeft 指的是root藏住的长度
      // 如果 thisLeft < -tabsLeft 代表这个li被藏在左边了
      // 那就直接把它放在左边第一个的位置
      if (thisLeft < -tabsLeft) {
        return root.css("left", -thisLeft);
      }
      // thisLeft + thisLi.outerWidth() 指的是li标签的尾部到root头部的距离
      // outerWidth - tabsLeft 指的是可视的尾部到root头部的距离
      // li被藏在了右边看不全
      if (thisLeft + thisLi.outerWidth() >= root.outerWidth() - tabsLeft) {
        // 计算被藏住的长度
        let subLeft =
          thisLeft + thisLi.outerWidth() - (root.outerWidth() - tabsLeft);
        root.children("li").each(function (i, item) {
          let li = $(item),
            left = li.position().left;
          if (left + tabsLeft > subLeft) {
            root.css("left", -left);
            return false;
          }
        });
      }
    },
    /**
     * 将内容向左边移动一个可视化距离
     */
    right: function (root, index) {
      let tabsLeft = parseFloat(root.css("left"));
      // left + li.outerWidth() li标签的位置
      // root.outerWidth() - tabsLeft 被展示到的最远位置
      // 将第一个在右边被遮住的li放在第一个展示
      root.children("li").each(function (index, item) {
        let li = $(item),
          left = li.position().left;
        if (left + li.outerWidth() >= root.outerWidth() - tabsLeft) {
          root.css("left", -left);
          return false;
        }
      });
    },
  };

  /**
   * 工具方法集合
   */
  var util = {
    /**
     * @method 获取{@linkplain config.pageDescribe 窗口描述配置项pageDescribe}
     * @param {String} key  窗口描述配置项pageDescribe里面的key值，如果不为空就返回key对应的值
     * @returns 窗口描述配置项pageDescribe或者它的成员
     */
    getPageDescribe(key) {
      return key ? config.pageDescribe[key] : config.pageDescribe;
    },

    /**
     * @method  获取{@linkplain config.temp.windowsMap 临时配置项windowsMap}
     * @param {String} key 临时配置项windowsMap里面的key值，如果不为空就返回key对应的值
     * @returns 临时配置项windowsMap或者它的成员
     */
    getWindowsMap(key) {
      return key ? config.temp.windowsMap[key] : config.temp.windowsMap;
    },

    /**
     * @method 获取{@linkplain config.pageMultiple 沙箱临时缓存关系表pageMultiple}
     * @param {*} key
     * @returns 沙箱临时缓存关系表pageMultiple或者它的成员
     */
    getPageMultiple(key) {
      return key ? config.pageMultiple[key] : config.pageMultiple;
    },

    /**
     * @method 获取指定的窗口的jq对象
     * @param {*} key 窗口的id(pageDescribe配置项里面的key)
     * @returns             当前窗口的jq对象
     */
    tabsBody(key) {
      return config.pageDescribe[key].parent;
    },

    /**
     * @method 防抖
     */
    debounce(fn, timer = "", params = {}) {
      config.timer[timer] && clearTimeout(config.timer[timer]);
      config.timer[timer] = setTimeout(() => {
        fn.apply(params.context, params.args || []);
      }, params.time || 300);
    },
  };

  /**
   * @namespace PROXY
   * @desc
   *
   *    获取特殊区域的jq对象
   *
   */
  let PROXY = {
    /**
     * @method 获取操作栏（li标签）
     *
     * @returns 操作栏jq对象
     */
    getActionContainer() {
      return $("#" + config.constant.HEADERS_ID);
    },

    /**
     * @method 获取最大化标记dom jq对象
     * @desc
     *
     *    在窗口最大化的时候，某些区域需要降低它的zIndex，这里来获取这些已标记的dom
     *
     * @returns 标记过的jq对象
     *
     */
    getMarkofMax() {
      return $("." + config.constant.MAX_CLASSNAME);
    },

    /**
     * @method 获取小窗口组
     * @returns
     */
    getTipsContainer() {
      return $("." + config.constant.CONTAINER_TIPS_CLASSNAME);
    },
  };

  let windowsProxy = {
    /**
     * 用户调用启动windows实例
     * @param {*} option
     * @param {*} list
     * @returns
     */
    wakeUp(option, list) {
      if (VM) return false;
      windowsProxy.initConfig(option, list);

      $body.append(
        $(`
        <div id = "${PROPERTY.MANAGE_CONTAINER_ID}">
          <windows></windows>
        </div>
      `)
      );
      VM = layui.binder({
        el: "#" + PROPERTY.MANAGE_CONTAINER_ID,
        components: {
          windows: "windows",
        },
        beforeDestroy() {
          $body.find("#" + PROPERTY.MANAGE_CONTAINER_ID).remove();
        },
        mounted() {
          //执行动画
          // if(config.constant.DYNAMIC_STARTER){
          //   the_container.animation();
          // }
        },
      });

      layui.layer.open = function (deliver, args0, args1) {
        // 拦截open方法，预处理参数
        if (
          window.layui &&
          layui.windows &&
          layui.windows.notify &&
          layui.windows.notify()
        ) {
          var tempResult = windowsProxy.open.call(
            VM.windows,
            deliver,
            args0,
            args1
          );
          // 判断结果是不是数字 说明返回的就是index，就直接返回
          if (/^\d+$/.test(String(tempResult))) return tempResult;
          // 没有就是参数已经被包装过了。直接进行下面的方法
          deliver = tempResult;
        }
        return openForever(deliver);
      };

      layui.layer.full = function (index) {
        // 获取配置项
        let wConfig = null;
        _lodash.every(util.getPageDescribe(), (v) => {
          if (v.index == index) {
            wConfig = v;
            return false;
          }
          return true;
        });
        if (!wConfig) return fullForever(index);
        // 执行默认的最大化回调函数
        // 先执行layui自带的方法，它回去记录恢复是的高宽
        let res = fullForever(index);
        windowsProxy.full.call(VM, wConfig);

        // 这里进行标记，声明是通过这种特殊方法处理以后的full
        // 在恢复窗口的时候就可以和min 区分开了
        wConfig.parent &&
          wConfig.parent
            .removeClass("layui-windows-full")
            .addClass("layui-windows-full");

        if (config.temp.resizeFn[wConfig.id]) {
          config.temp.resizeFn[wConfig.id].call(VM);
        }
        return res;
      };

      layui.layer.min = function (index) {
        // 获取配置项
        let wConfig = null;
        _lodash.every(util.getPageDescribe(), (v) => {
          if (v.index == index) {
            wConfig = v;
            return false;
          }
          return true;
        });
        // 执行默认的最小化回调函数
        if (!wConfig) return minForever(index);
        /**
         * 使用layui原生的方法，多次min总是会出错，所以这个直接修改这个min方法
         * 设置两个class
         * {@linkplain config.constant.MAX_ACTION_CLASS 模拟最小化的class类} 添加这个class
         * {@linkplain config.constant.MIN_ACTION_CLASS 模拟恢复最小化的class类} 添加这个class
         *
         * 首先如果有 MAX 就移除 MAX 干扰，因为 MAX仅仅是说通过 restore 恢复了 ，但是这里要达到的效果是min方法之后继续min没得影响。
         * 如果当前窗口已经标记恢复 MIN 了就不再继续执行了，这个窗口已经最小化了不用继续最小化了
         * 因为接下来会有一个向下的动画所以需要记录当前的top值
         * 给这个窗口添加一个 MIN 的类标志它最小化，执行最小化动画(动画最后隐藏窗口)
         * 执行min回调
         */
        if (wConfig.parent) {
          if (wConfig.parent.hasClass(config.constant.MAX_ACTION_CLASS))
            wConfig.parent.removeClass(config.constant.MAX_ACTION_CLASS);
          if (wConfig.parent.hasClass(config.constant.MIN_ACTION_CLASS)) return;
          // 记录top
          if (!util.getWindowsMap(wConfig.id).realTop)
            util.getWindowsMap(wConfig.id).realTop = parseInt(
              wConfig.parent.css("top")
            );
          // 记录 left
          if (!util.getWindowsMap(wConfig.id).realLeft)
            util.getWindowsMap(wConfig.id).realLeft = parseInt(
              wConfig.parent.css("left")
            );
          // 计算 left
          let liDom = document.querySelector(
            '[lay-id="' + util.getWindowsMap(wConfig.id).parentid + '"]'
          );
          let currentLeft = liDom
            ? parseInt(liDom.getBoundingClientRect().left)
            : 0;
          if (
            util.getWindowsMap(wConfig.id).realLeft > currentLeft &&
            util.getWindowsMap(wConfig.id).realLeft - currentLeft > 200
          )
            currentLeft = util.getWindowsMap(wConfig.id).realLeft - 200;
          if (
            util.getWindowsMap(wConfig.id).realLeft < currentLeft &&
            currentLeft - util.getWindowsMap(wConfig.id).realLeft > 200
          )
            currentLeft = util.getWindowsMap(wConfig.id).realLeft + 200;
          wConfig.parent.addClass(config.constant.MIN_ACTION_CLASS);
          wConfig.parent.animate(
            {
              top: $body.height() - wConfig.parent.height() - 50,
              left: currentLeft,
              // opacity: 0.2,
              transform: "scale(.6)",
            },
            50,
            function () {
              wConfig.parent
                .removeClass(config.constant.HIDE)
                .addClass(config.constant.HIDE);
            }
          );
          windowsProxy.min.call(VM, wConfig);
        }
        // return res;
      };

      layui.layer.restore = function (index) {
        // 获取配置项
        let wConfig = null;
        _lodash.every(util.getPageDescribe(), (v) => {
          if (v.index == index) {
            wConfig = v;
            return false;
          }
          return true;
        });
        if (!wConfig) return restoreForever(index);

        /**
         * 如果这个窗口有layui-windows-full说明是最大化的恢复：移除这个class之后按照最大化恢复的方式处理
         * 和min方法类似，移除MIN 类标志防止干扰
         * 如果已经标志MAX恢复过了就不继续向下执行了
         * 显示窗口并添加一个恢复标志 MAX
         * 执行恢复动画
         * 执行恢复回调
         */
        if (wConfig.parent) {
          if (wConfig.parent.hasClass("layui-windows-full")) {
            wConfig.parent.removeClass("layui-windows-full");
            windowsProxy.restore.call(VM, wConfig);
            let res = "";
            try {
              res = restoreForever(index);
            } catch (error) {
              //可能出错，暂且忽略
            }
            // 执行窗口resize事件;
            if (config.temp.resizeFn[wConfig.id]) {
              config.temp.resizeFn[wConfig.id].call(VM);
            }
            return res;
          }
          // windowsProxy.restore.call(VM, wConfig);
          if (wConfig.parent.hasClass(config.constant.MIN_ACTION_CLASS))
            wConfig.parent.removeClass(config.constant.MIN_ACTION_CLASS);
          if (wConfig.parent.hasClass(config.constant.MAX_ACTION_CLASS)) return;
          wConfig.parent
            .removeClass(config.constant.HIDE)
            .addClass(config.constant.MAX_ACTION_CLASS);
          wConfig.parent.animate(
            {
              top: util.getWindowsMap(wConfig.id).realTop,
              left: util.getWindowsMap(wConfig.id).realLeft,
              // opacity: 1,
              transform: "scale(1)",
            },
            50,
            function () {}
          );
          windowsProxy.restore.call(VM, wConfig);
          if (config.temp.resizeFn[wConfig.id]) {
            config.temp.resizeFn[wConfig.id].call(VM);
          }
        }
      };
    },

    /**
     * @method 初始化配置参数
     * @param {*} option   静态配置参数
     * @param {*} list     窗口描述参数
     */
    initConfig(option = {}, list = {}) {
      _lodash.assign(config.constant, option);
      // 替换第三方登录用户名
      if (config.constant.DYNAMIC_OWNER)
        config.base.username = config.constant.DYNAMIC_OWNER;
      _lodash.each(list, (v, k) => (config.pageDescribe[k] = v));
    },

    /**
     * 判断是否已经启用windows实例
     */
    notify() {
      return VM && VM.isAlive;
    },

    /**
     * @global
     * @public
     * @method open 在打开一个图层，装饰参数(核心方法)
     *
     * @todo
     * {@link layer.open}  ### 需要在这个方法之前拦截参数进行修饰
     *
     * @param {*} opt pageDescribe配置项里面的key或者里面的id,现在也可以直接是layer.open 的配置项参数
     * @param {Object/Function} data 传入的参数，或者一个回调函数，回调函数的参数config是对应的pageDescribe配置项
     * @returns  1.返回的是一个配置项参数，用来供layer.open方法继续处理 2.窗口已打开返回index，这也是原open方法的返回值
     * @since v1.0.0
     * @desc
     *
     *    这个方法里面的this指向的是对应的视图对象
     *
     *    过程：
     *
     *       1. 过滤加载层和tips层(这种图层无需集中管理)，带遮罩层的也过滤掉，已设置皮肤和忽略项的忽略
     *       2. 判断非配置项的参数传入: 如果传入的是字符串那么会根据需要去补全配置参数
     *       3. 已描述为打开的窗口直接返回并调用回调函数
     *       4. 补全参数
     *       5. 如果是沙箱模式就生成临时沙箱配置参数
     *       6. 装饰success、end、cancel、full、restore、resizing等回调函数
     *       7. 如果没有content生成content
     *       8. 将参数返回。layui.layer.open接手参数并弹出窗口
     */
    open(opt = {}, data = {}) {
      // 过滤掉加载层和tips层
      if (opt.type == "3" || opt.type == "4") return opt;
      // 过滤掉带遮罩的窗口
      if (opt.shade) return opt;
      // 特殊窗口有皮肤的跳过,加上当前皮肤的检验，下面都替换成了这个
      if (opt.skin && opt.skin !== "layui-layer-windows") return opt;
      // 显式的声明忽略的
      if (opt.ignore) return opt;
      /**
       * 参数1 opt
       * 如果只传入了一个字符串，那么是通过key来获取值，或者自动生成一个临时的配置项  temp = true
       */
      if (_lodash.isString(opt)) {
        opt = util.getPageDescribe(opt)
          ? util.getPageDescribe(opt)
          : {
              id: opt,
              name: opt,
              type: 1,
              temp: true,
            };
      } else {
        // 经过观察msg的type为空，需要排除
        if (opt.type === undefined) return opt;
      }
      /**
       * 如果其它情况传入的参数中没有明确窗口id的,也要生成窗口id并标记为临时窗口
       */
      if (!opt.id) {
        opt.id = "TEMP_" + config.constant.INTERNAL_INDEX++;
        opt.temp = true;
      }

      /**
       * 到这里id算是落地了，首先要检查窗口是否已经打开了
       */
      if (util.getPageDescribe(opt.id) && util.getPageDescribe(opt.id).index) {
        // 置顶这个窗口
        this.setTop(opt.id);
        // 执行回调函数
        _lodash.isFunction(data) && data(util.getPageDescribe(opt.id));
        // 返回当前窗口的index
        return util.getPageDescribe(opt.id).index;
      }
      /**
       * 补全参数，并将参数放入 {@linkplain config.pageDescribe 窗口描述对象} 中管理
       * 在这里将参数补全
       */
      // skin  这里需要强制替换成 layui-layer-windows
      opt.skin = "layui-layer-windows";
      // icon 这个属性是 信息框和加载层的私有参数 加载时添加一个图标(但是在这里的特殊需求是一个代表窗口的layui图标)
      // icon 属性是一个class name 默认是一个文件的图标  layui-icon-file
      if (opt.icon === undefined) opt.icon = "layui-icon-file";
      // v4.2.0 新增     为上面的icon添加一个控制颜色的变量 iconColor 默认值是 rgba(var(--windows-main-button-color), 1)
      if (opt.iconColor === undefined)
        opt.iconColor = "rgba(var(--windows-main-button-color), 1)";
      // shade属性是强制关闭的
      opt.shade = 0;
      // maxmin属性默认是开启的,有最小化隐藏的特殊前提
      if (opt.maxmin === undefined) opt.maxmin = true;
      // fixed 属性必须关闭，当前layer对这个属性还有隐藏bug未修复
      opt.fixed = false;
      // 补全 name 属性
      if (opt.name === undefined) opt.name = opt.id;
      // 补全title属性
      if (opt.title == undefined)
        opt.title = `${
          opt.icon === undefined
            ? ""
            : `<i class="layui-icon ${opt.icon}"></i>&nbsp;`
        }<b>${opt.name}</b>`;
      /**
       * @since v4.2.0 在信息落地之前，检查它是否在配置项里面
       * 如果没有的话就需要尝试将这个配置项先加入到主菜单配置项中
       */
      if (!util.getPageDescribe(opt.id)) {
        opt.pid && windowsProxy.joinInMenu(opt);
      }
      util.getPageDescribe()[opt.id] = opt;
      /**
       * 获取当前窗口的窗口描述对象
       */
      let wConfig = util.getPageDescribe(opt.id);

      /**
       * @since v4.2.0 新增
       * @desc
       *    判断这个配置项里面是否是标记了阻止继续调用open方法,
       *    如果它的配置项 mark 不为空的，就阻止向下面进行，在这个函数里面返回 0 吧
       */
      if (!!wConfig.mark) {
        // TODO 打开主菜单并定位到这个菜单
        // 返回一个数字，它就不会去调用open方法，而是直接返回这个数字
        return 0;
      }
      /**
       * 判断沙箱情景：
       *    1. 非临时窗口  wConfig.temp !== true
       *    2. 配置项中启用沙箱模式  wConfig.porperty === property
       */
      var multipleFlag =
        wConfig.temp !== true && wConfig.porperty === "property";
      if (multipleFlag) {
        /**
         * 获取沙箱编号
         *
         *    参数2 允许传入一个额外的配置项。从这个配置项中获取id或code作为沙箱编号
         *    将沙箱编号和原配置信息管理放入 {@linkplain config.pageMultiple 沙箱临时缓存关系表} 中
         *    与沙箱缓存中能匹配到的情况，与 "已打开的页面无需重新打开" 相同处理
         */
        let key = data.id || data.code || 0;
        // 查看沙箱缓存
        if (util.getPageMultiple(wConfig.id)) {
          // 寻找并匹配沙箱编号
          if (key && util.getPageMultiple(wConfig.id)[key]) {
            // 匹配到就按照 -- >  "已打开的页面无需重新打开" 处理
            // 置顶这个窗口
            this.setTop(util.getPageMultiple(wConfig.id)[key]);
            // 获取沙箱临时配置项
            let sConfig = util.getPageDescribe(
              util.getPageMultiple(wConfig.id)[key]
            );
            // 如果第二个参数是回调函数就执行这个回调函数
            _lodash.isFunction(data) && data(sConfig);
            return sConfig.index;
          }
        } else {
          // 没有沙箱缓存就需要创建
          util.getPageMultiple()[wConfig.id] = {};
        }
        // 生成临时key or id
        let _key = "TEMP_" + config.constant.INTERNAL_INDEX++;
        // 如果有沙箱编号就将沙箱编号信息也加入
        if (key) {
          util.getPageMultiple(wConfig.id)[key] = _key;
        }
        /**
         * 组装生成沙箱配置项并加入pageDescribe配置对象中
         * 1.name属性修改，根据追加参数的name属性进行改变
         * 2.将传入的index记录为topid
         * 3.申明这个临时配置对象是一个子对象
         */
        util.getPageDescribe()[_key] = _lodash.assign({}, wConfig, {
          id: _key,
          name:
            data && data.name ? wConfig.name + "-" + data.name : wConfig.name,
          topid: wConfig.id,
          porperty: "child",
        });
        /**
         * 将这个外面确定的 wConfig 配置对象
         */
        wConfig = util.getPageDescribe(_key);
      }
      // 合并参数，构建layui.layer.open 所需要的参数
      let option = _lodash.cloneDeep(wConfig);
      let self = this;
      //接下来装饰一系列的回调方法
      /**
       * 窗口成功打开之后的回调函数
       * @param {*} layero   如果是弹出窗口层，就是窗口的jq对象
       * @param {*} index  layui.layer.open的返回结果，弹层的id
       * @param {*} layopt  layui.layer的配置对象，将会在layui2.8版本出现，这里先占好位子
       */
      option.success = function (layero, index, layopt) {
        // 新增转换，如果是type = 2 说明是iframe层
        if (wConfig.type == 2)
          layero = layui.layer.getChildFrame("body", index);

        // 执行默认的回调方法
        windowsProxy.success.call(self, layero, index, layopt, wConfig);

        // 新增，如果要求来解析template就帮忙解析一下，将content返回
        if (wConfig.type == 1 && !!wConfig.autoBuild) {
          /**
           * v4.2.0 新增判断，如果在参数初始化阶段就已经产生和视图对象，这里就不再进行创建了
           */
          if(!wConfig.VM || (wConfig.VM && !wConfig.VM.isAlive)){
            let _param = windowsProxy.packOption(wConfig.autoBuild);
            // 将layero窗口对象传入  传dom
            _param.el = layero.get(0).querySelector('.layui-layer-content');
            // 创建视图对象，并放入wConfig中
            wConfig.VM = layui.binder(_param);
          }
        }

        // 执行成功后的回调函数
        // 新增限制条件，仅type=1的才能行
        if (
          wConfig.type == 1 &&
          wConfig.moudle &&
          _lodash.isString(wConfig.moudle)
        ) {
          layui.use(wConfig.moudle, function () {
            if (layui[wConfig.moudle][config.constant.METHOD_AUTOEXEC_NAME])
              layui[wConfig.moudle][config.constant.METHOD_AUTOEXEC_NAME](
                layero,
                index,
                layopt,
                data,
                wConfig
              );
            if (layui[wConfig.moudle].resize)
              windowsProxy.resize(wConfig.id, layui[wConfig.moudle].resize);
            _lodash.isFunction(data) && data(wConfig);
          });
        } else {
          _lodash.isFunction(data) && data(wConfig);
        }

        // 执行传入的配置项参数里面用户自定义的成功回调函数
        wConfig.success && wConfig.success(layero, index, layopt);
      };
      // yes cancel 回调跳过

      /**
       * 窗口关闭之后的回调函数
       */
      option.end = function () {
        // 执行默认的关闭回调函数
        windowsProxy.end.call(self, wConfig);

        // 执行销毁回调函数
        if (wConfig.moudle && _lodash.isString(wConfig.moudle)) {
          if (layui[wConfig.moudle].destroy) {
            layui[wConfig.moudle].destroy(wConfig.id, wConfig);
          }
          if (layui[wConfig.moudle].resize) {
            windowsProxy.resize(wConfig.id);
          }
          if (wConfig.VM) {
            wConfig.VM.isAlive === true && wConfig.VM.$destroy();
            wConfig.VM = null;
          }
        }

        // 执行传入的配置项参数里面用户自定义的失败回调函数
        wConfig.end && wConfig.end(wConfig.id);
      };

      /**
       * layui.layer原生窗口resize事件回调函数
       */
      option.resizing = function (layero) {
        // 执行默认的resize事件回调函数
        windowsProxy.resizing.call(self, wConfig);
        wConfig.resizing && wConfig.resizing(layero);
        // 执行窗口resize事件
        if (config.temp.resizeFn[wConfig.id]) {
          config.temp.resizeFn[wConfig.id].call(self);
        }
      };

      option.moveEnd = function (layero) {
        util.getWindowsMap(wConfig.id).realTop = parseInt(layero.css("top"));
        util.getWindowsMap(wConfig.id).realLeft = parseInt(layero.css("left"));
        wConfig.moveEnd && wConfig.moveEnd(layero);
      };

      // 处理content
      if (!option.url && !option.content)
        option.content = "<div>" + option.name + "</div>";
      return option;
    },

    /**
     * @method 置顶窗口
     * @desc
     *    this指向的是对应的视图对象
     * @param {*} id 窗口id
     */
    setTop(id) {
      if (!id) return;
      // 执行(置顶窗口)前置事件
      windowsProxy.beforeSetTop.call(this);
      /**
       * 获取当前最大的层级
       */
      let zIndex = config.constant.MAX_INDEX;

      /**
       * 更新zIndex值，达到结果最近访问的窗口排在上面(预留最顶层的位置)
       * 1.根据当前更新时间，由近时间到远时间对 windowsMap 进行排序
       * 2.在1的顺序下遍历它，并更新每个窗口的zIndex的值
       */
      _lodash.each(
        _lodash.sortBy(util.getWindowsMap(), (u) => -u.updateTime),
        function (v) {
          util.tabsBody(v.id).css("z-index", --zIndex);
          // 记录当前的层级，在windows预览恢复的时候恢复为该层级
          v.nowIndex = zIndex;
        }
      );

      /**
       * 遍历活动组池(更新select选中状态):
       *
       *    除了index在children列表里面的分组更新为选中，其它的分组切换成未选中状态
       *
       */
      _lodash.each(this.livelyPool, function (v) {
        let childernflag = false;
        _lodash.each(v.children, function (v1) {
          if (v1 == id) childernflag = true;
        });
        v.select = childernflag;
      });

      /**
       * 更新当前置顶窗口的配置项最近活跃时间
       */
      util.getWindowsMap(id).updateTime = new Date();

      /**
       * 将当前窗口的z-index设置为最高
       * 将当前窗口上面的隐藏class去掉
       */
      util
        .tabsBody(id)
        .css("z-index", config.constant.MAX_INDEX)
        .removeClass(config.constant.HIDE);
      util.getWindowsMap(id).nowIndex = config.constant.MAX_INDEX;

      /**
       * 如果当前窗口最小化，恢复窗口
       */
      if (util.getWindowsMap(id).min)
        layui.layer.restore(util.getPageDescribe(id).index);

      util.tabsBody(id).removeClass(config.constant.MIN_ACTION_CLASS);

      /**
       * 更新topIndex
       */
      config.constant.TOP_INDEX = id;

      /**
       * 选项卡滚动到合适的位置
       */
      this.rollPageAuto(id);

      // 执行(置顶窗口)后置事件
      windowsProxy.afterSetTop.call(this);
    },

    /**
     * @method 置顶窗口-前置事件
     */
    beforeSetTop() {
      //  TODO  置顶时，如果有打开3d flip的场合 关闭3d flip
    },

    /**
     * @method 置顶窗口-后置事件
     */
    afterSetTop() {},

    /**
     * @method 添加resize 事件
     * @param {*} id  事件的分组id,这里推荐使用窗口id(pageDescribe配置项里面的key)
     * @param {*} fn  resize回调函数(), 事件的分组id会作为参数传入
     */
    resize: function (id, fn) {
      // 如果之前已经绑定过了，就进行解绑操作
      if (config.temp.resizeFn[id]) {
        $(window).off("resize", config.temp.resizeFn[id]);
        delete config.temp.resizeFn[id];
      }
      //如果是清除 resize 事件，则终止往下执行
      if (!_lodash.isFunction(fn)) return;
      // 添加resize事件
      config.temp.resizeFn[id] = function () {
        util.debounce(fn, "resize" + id, { args: [id] });
      };
      $(window).on("resize", config.temp.resizeFn[id]);
    },

    /**
     * @method 窗口成功打开之后的默认回调函数
     * @desc
     *
     *    这个方法里面的this指向的是对应的视图对象
     *
     * @param {*} layero    如果是弹出窗口层，就是窗口的jq对象
     * @param {*} index     layui.layer.open的返回结果，弹层的index
     * @param {*} layopt    保留参数
     * @param {*} wConfig   窗口对应的pageDescribe配置项
     */
    success(layero, index, layopt, wConfig) {
      /**
       * 1. 设置窗口配置项的初始值
       */
      // 在窗口配置对象中写入layui返回结果，表明这个窗口已经被打开，可以使用
      wConfig.index = index;
      // 在窗口配置对象中写入layui窗口的jq对象方便后面的调用
      // ** 弹出窗口层这里就可以了，弹出的是iframe层就需要自己调方法进行处理了
      wConfig.parent = layero;
      /**
       * 更新最大弹层层级
       * @since v2.0.0
       */
      config.constant.MAX_INDEX = parseInt(layero.css("zIndex"));

      /**
       * 2. 下面进行窗口组管理
       */
      // 获取父ID
      let parentid = wConfig.topid || wConfig.id;
      // 获取父名称
      let parentname = util.getPageDescribe(parentid).name;
      //加入windows描述中
      util.getWindowsMap()[wConfig.id] = {
        id: wConfig.id,
        parentid: parentid,
        name: wConfig.name,
        select: false,
        min: false,
        show: true,
        offsetTop: parseInt(layero.css("top")),
        offsetLeft: parseInt(layero.css("left")),
        updateTime: new Date(),
      };
      // 检查父id是否存在
      // 可能这个分组下面的其它窗口声明时已经添加了父id的声明
      let parentflag = false;
      _lodash.each(this.livelyPool, function (v) {
        if (v.id == parentid) {
          // 如果找到对应的父id就将这个标记置为true
          parentflag = true;
          let childflag = false;
          // 遍历父id对应的children ，只有children里面没有当前的id才在里面加入
          _lodash.each(v.children, function (v1) {
            if (v1 == wConfig.id) childflag = true;
          });
          if (!childflag) v.children.push(wConfig.id);
        }
      });
      // 如果没有找到父id对应的内容，这里就需要立即创建
      if (!parentflag)
        this.livelyPool.push({
          index: config.constant.INTERNAL_INDEX++,
          id: parentid,
          name: parentname,
          select: false,
          icon: wConfig.icon,
          iconColor: wConfig.iconColor,
          children: [wConfig.id],
          fixed: false,
        });
      let self = this;
      // 窗口点击置顶事件，点击一下就setTop了
      /**
       * 这个是修改layui.layer里面原生的setTop方法
       * 实现一种点击窗口，那这个窗口就自动切换成最顶层层级的窗口
       * 达到一种窗口切换的效果
       *
       * v1.1.2 修改成mousedown的事件可以将这个事件直接放到整个窗体上面
       */
      if (!wConfig.click) {
        wConfig.click = true;
        layero.on("mousedown", function () {
          self.setTop(wConfig.id);
        });

        // 添加窗口移动的时候判断事件
        layero.on("mousedown", ".layui-layer-title", function (e) {
          // 捕获当前的窗口
          windowsProxy.captureWindow(wConfig);
        });
      }

      // 最后setTop一下,防止其它操作的影响，保持新开的窗口处于最顶层级
      this.setTop(wConfig.id);
    },

    /**
     * @method 捕获某个窗口
     * @desc
     *
     *    1. 首先要将 {@linkplain config.temp.windowsMap 窗口弹层描述对象}  里面的当前的offsetTop,和offsetLeft 重新记录
     *    2. 将id和窗口对象放入 {@linkplain config.temp.disposeWindow 捕获窗口对象描述} 中
     *
     * @param {*} wConfig  窗口的配置对象, pageDescribe配置
     */
    captureWindow(wConfig) {
      let id = wConfig.id;
      let layero = util.getPageDescribe(id).parent;
      if (layero) {
        util.getWindowsMap(id).offsetTop = parseInt(layero.css("top"));
        util.getWindowsMap(id).offsetLeft = parseInt(layero.css("left"));
        config.temp.disposeWindow = {
          id: id,
          layero: layero,
        };
      }
    },

    /**
     * @method 释放窗口
     */
    releaseWindow() {
      if (config.temp.disposeWindow.id) config.temp.disposeWindow = {};
    },

    /**
     * @method 窗口销毁之后的回调
     * @param {*} id  窗口id
     */
    end(wConfig) {
      let id = wConfig.id;
      /**
       * 处理窗口组临时窗口对象
       * 标记了topid,说明这个是一个临时沙箱配置对象，topid的值是它对应的父对象的id
       */
      if (wConfig.topid) {
        let topid = wConfig.topid;
        // 父对象的临时复合参数如果没有创建就自动创建(一般是有的，就怕特殊情况报错)
        if (!util.getPageMultiple(topid)) util.getPageMultiple()[topid] = {};
        /**
         * 遍历 {@linkplain config.pageMultiple 沙箱临时缓存关系表}
         *      如果有和当前的id匹配上的，就将它删除
         */
        _lodash.every(util.getPageMultiple(topid), function (v, k) {
          if (v == id) {
            delete config.pageMultiple[topid][k];
            return false;
          }
          return true;
        });
      }
      // 在 windowsMap 配置项里面移除这一项
      if (util.getWindowsMap(id)) delete config.temp.windowsMap[id];

      // 在窗口组中移除这个窗口
      let groupid = util.getPageDescribe(id).topid || id;
      _lodash.remove(this.livelyPool, function (v) {
        // 无关的分组直接返回false来保留
        if (v.id != groupid) return false;
        // 移除分组下面该id匹配的项
        _lodash.remove(v.children, (v1) => v1 == id);
        // 如果分组的children长度为0就移除这个分组,并且没有固定在操作栏上面的分组
        return v.children.length == 0 && v.fixed === false;
      });

      // TODO 根据情况确定下一个top的窗口，这里先将置顶窗口设置为0
      config.constant.TOP_INDEX = 0;

      // 移除hover预览小窗口中的这一项
      _lodash.remove(this.hoverPool, (v) => v.id == id);

      // 移除配置参数中已经添加了点击事件的标记，允许下一次打开的新窗口添加点击事件
      wConfig.click = false;
      // 最后再移除,新增的临时配置参数
      if ("child" == wConfig.porperty) {
        // 这个说明是临时窗口直接移除
        delete config.pageDescribe[id];
      } else {
        // 一般的将配置参数还原
        wConfig.index = null;
        wConfig.parent = null;
        if (wConfig.VM) wConfig.VM = null;
      }

      // 恢复最大化添加的class
      if (document.querySelectorAll(".layui-windows-full").length == 0)
        PROXY.getMarkofMax().removeClass(config.constant.MIN_CLASSNAME);
    },

    /**
     * @method 窗口最大化之后的回调
     * @param {*} wConfig
     */
    full(wConfig) {
      //TODO 关闭3dflip
      /**
       * 窗口最大化了，但是它的层级并不是最高的。
       *    这个时候需要去调整这些区域的zIndex防止它们遮挡窗口
       *    这些区域被 {@linkplain config.constant.MAX_CLASSNAME 最大化修饰样式 } 的class所修饰
       *    这里需要给这些区域添加 {@linkplain config.constant.MIN_CLASSNAME 反最大化修饰样式 }的class
       */
      PROXY.getMarkofMax()
        .removeClass(config.constant.MIN_CLASSNAME)
        .addClass(config.constant.MIN_CLASSNAME);
    },

    /**
     * @method 窗口最小化之后的回调
     * @param {*} wConfig
     */
    min(wConfig) {
      //TODO 关闭3dflip
      // 这个在setTop的操作时有对应的 处理
      // wConfig.parent
      //   .removeClass(config.constant.HIDE)
      //   .addClass(config.constant.HIDE);
      util.getWindowsMap(wConfig.id).min = true;
      util.getWindowsMap(wConfig.id).show = false;
      util.getWindowsMap(wConfig.id).select = false;

      // 最小化之后当前置顶id就不是它了
      if (config.constant.TOP_INDEX == wConfig.id)
        config.constant.TOP_INDEX = 0;
    },

    /**
     * @method 窗口恢复之后的回调
     * @param {*} wConfig
     */
    restore(wConfig) {
      /**
       * 这个窗口取消最大化状态
       *
       *    因为窗口最大化的时候，有些dom会特殊的调整它们的zIndex,这里关闭了就反向调整回来
       */
      PROXY.getMarkofMax().removeClass(config.constant.MIN_CLASSNAME);
      // wConfig.parent.removeClass(config.constant.HIDE);
      util.getWindowsMap(wConfig.id).min = false;
      util.getWindowsMap(wConfig.id).show = true;
      util.getWindowsMap(wConfig.id).select = true;
    },

    /**
     * @method 窗口resizing之后的回调
     * @param {*} wConfig
     */
    resizing(wConfig) {
      //TODO 关闭3dflip
    },

    /**
     * @method 最小化当前全部窗口
     */
    minAll() {
      _lodash.each(util.getWindowsMap(), (v, k) => {
        if (v.min === false) {
          util.getPageDescribe(k) &&
            util.getPageDescribe(k).index &&
            layui.layer.min(util.getPageDescribe(k).index);
        }
      });
    },

    /**
     * @method 关闭当前全部窗口
     */
    closeAll() {
      _lodash.each(util.getWindowsMap(), (v, k) => {
        util.getPageDescribe(k) &&
          util.getPageDescribe(k).index &&
          layui.layer.close(util.getPageDescribe(k).index);
      });
    },

    /**
     * @method 组装binder的参数
     * @param {*} build    传入的binder配置项
     * @param {*} template 指定的模板，没有指定就从第一个参数里面找
     * @returns {*} option配置项
     * @since v4.2.0
     */
    packOption(build, template){
      let _param = {};
      // 初始化属性
      _param.data = build.data ? build.data : {};
      _param.dataSource = build.dataSource ? build.dataSource : {};
      _param.computeds = build.computeds ? build.computeds : {};
      // 初始化指令
      _param.diretives = build.diretives ? build.diretives : {};
      // 初始化监视属性
      _param.watchs = build.watchs ? build.watchs : {};
      // 初始化方法
      _param.methods = build.methods ? build.methods : {};
      // 初始化组件
      _param.components = build.components ? build.components : {};
      // 初始化模版
      if(template){
        _param.template = template;
      }else{
        _param.template =  build.template ? build.template : null;
      }
      return _param;
    },

    /**
     * @method 展示小窗口
     * @desc
     *
     *    选项dom移上去的监听事件
     *    就是鼠标在操作栏的li标签上面移动的时候，需要弹出一个小窗口，上面有这个分组下面的子窗口
     *      这个窗口的位置和内容的改变，我先放在这个监听事件里面进行，因为是只有在这个li标签发生改变的时候才需要去改变
     * @param {*} e
     */
    showWindowTip(e) {
      /**
       * 1. 首先排除固定的情况。
       *     - 这个是因为在执行点击li标签选项的时候会去执行它的点击回调函数 {@linkplain windowsProxy.setGroupTop  点击选项置顶}
       *    在这个回调函数里面，如果这个li标签对应的分组，它下面的子窗口超过一个，那么不会将子窗口置顶，而是将这个弹出的窗口固定，让用户去选择是置顶哪个窗口
       *    这个时候的设计就是这个窗口固定在这里，切换li标签的时候也不能去影响它，所以这里如果遇到固定的情况就返回。
       */
      // 获取操作栏上面的小窗口集合的dom元素
      let $target = PROXY.getTipsContainer.call(this);
      // 固定了就不再向下处理了
      if ($target.hasClass(config.constant.FIXED_LI_TAG_CLASSNAME)) return;
      /**
       * 2. 获取当前待检阅的分组id
       *    由于binder模板本身的问题这里是将分组id作为一个属性防置在dom上的
       *   所以这里要先去获取到这个属性，然后才能确定检阅哪个分组
       */
      let id = e.target.getAttribute("lay-id");
      /**
       * 整理如果获取到的id 和记录下的 {@linkplain config.temp.hoverGroupID 检阅分组的id} 相等时要返回。防止重复渲染
       * 如果没有能获取到这个属性值说明鼠标可能是指到了其它地方触发了这个回调，这个时候应该忽略
       *  - 如果是使用jQuery的attr方法获取属性，为空是 undefined
       *  - 如果是原生的HTMLElement的getAttribute方法获取属性，为空是 null
       */
      if (id === undefined || id === null) return;
      if (id === config.temp.hoverGroupID) {
        if (!$target.hasClass(config.constant.DISPLAY_LI_TAG_CLASSNAME))
          $target.addClass(config.constant.DISPLAY_LI_TAG_CLASSNAME);
        return;
      }
      // 记录下当前的id
      config.temp.hoverGroupID = id;
      let hoverPool = [];
      /**
       * 3.遍历整个动态分组，将找到的分组放入 hoverPool 中渲染出各个预览小窗口
       */
      _lodash.every(this.livelyPool, function (v) {
        if (v.id != id) return true;
        // 将分组中的children对应的配置参数写入到 hoverPool 中
        _lodash.each(v.children, (v1) =>
          hoverPool.push({
            id: util.getWindowsMap(v1).id,
            name: util.getWindowsMap(v1).name,
            groupid: id,
          })
        );
        return false;
        // 位置的调整放到监视属性里面去做，因为后面可能涉及分组的增加和删除
      });
      this.hoverPool = hoverPool;
      /**
       * 对$target进行css样式的修饰
       * 1. 添加 {@linkplain config.constant.DISPLAY_LI_TAG_CLASSNAME  select-tip} 将这个区域显示在靠近底部
       * 2. 放在后面是因为现在如果list为0 -> 1 这个ele会触发动态class的刷新，会覆盖class
       * 3. 只要鼠标是放在操作栏的li标签上面的时候通过这个class来保持小窗口展示，
       *    在小窗口上面移动的时候通过另外的一个class保持小窗口展示
       *
       */
      if (!$target.hasClass(config.constant.DISPLAY_LI_TAG_CLASSNAME))
        $target.addClass(config.constant.DISPLAY_LI_TAG_CLASSNAME);
    },

    /**
     * @method 保持小窗口
     * @desc
     *
     *    操作栏dom移动时或者小窗口移动时的监听事件
     *    鼠标在操作栏上面移动时或者小窗口移动时应该检查，保持显示 -  添加class select-tip-flag
     *    但是正在动画过程中可以忽略（修改: 不可以! 否则会在快速操作时不能添加class导致关闭和显示时出现闪动）
     */
    hoverTip() {
      let $target = PROXY.getTipsContainer.call(this);
      if (!$target.hasClass("select-tip-flag"))
        $target.addClass("select-tip-flag");
    },

    /**
     * @method 关闭小窗口
     * @desc
     *
     *    操作栏dom移开时的监听事件
     *    当鼠标离开操作栏上面的li标签的时候会触发。移除样式，让小窗口不再展示
     *    这个关闭窗口的事件是针对上面的 {@linkplain windowsProxy.showWindowTip li标签展示小窗口回调的 }
     *    所以这里移除的样式也是 {@linkplain config.constant.DISPLAY_LI_TAG_CLASSNAME  select-tip}
     *
     */
    hideWindowTip() {
      let $target = PROXY.getTipsContainer.call(this);
      $target.removeClass(config.constant.DISPLAY_LI_TAG_CLASSNAME);
      // 新增调用方法将hoverPool重置
      windowsProxy.removeGroup.call(this);
    },

    /**
     * @method 通过小窗口关闭页面
     * @desc
     *
     *    预览小窗口的右上方有一个红叉，点击这里可以关闭对应的窗口
     *
     *    关闭之后需要隐藏小窗口:
     *      因为关闭窗口之后会去修改windowsMap和livelyPool,
     *      再次触发检索的时候就会重新的去获取hoverPool列表
     *      这种方式才能更新预览列表
     *
     * @todu 如果设置了hover组id的缓存，这里需要删除这个缓存，因为这个执行之后需要强制去更新hoverPool
     * @param {*} e
     */
    closeFromTip(e) {
      e.stopPropagation();
      e.preventDefault();
      /**
       * 1. 获取当前预览小窗口指代的窗口描述id，要使用这个id能确认当前需要预览的窗口到底是哪一个
       */
      let id = e.target.getAttribute("wid");
      // 没有获取到id说明指到了别的地方需要排除
      if (id === undefined || id === null) return;
      // 关闭窗口
      if (util.getPageDescribe(id) && util.getPageDescribe(id).index) {
        layui.layer.close(util.getPageDescribe(id).index);
        // 清除预览参数,先
        this.hover = { id: 0 };
        // 新增调用方法将hoverPool重置
        util.debounce(windowsProxy.removeGroup, "appendHover", {
          time: 450,
          context: this,
        });
      }
    },

    /**
     * @method 点击预览小窗口置顶它对应的窗口
     */
    setHoverTop(e) {
      /**
       * 1. 获取当前预览小窗口指代的窗口描述id，要使用这个id能确认当前需要预览的窗口到底是哪一个
       */
      let id = e.target.getAttribute("wid");
      // 没有获取到id说明指到了别的地方需要排除
      if (id === undefined || id === null) return;
      // 调用方法置顶
      windowsProxy.setTop.call(this, id);
      // 清除预览参数
      this.hover = { id: 0 };
      // 添加，重置预览时添加的临时参数，防止最小化的影响
      util.getWindowsMap(id).hoverMin = null;
      // 阻止事件冒泡
      if (window.event) window.event.stopPropagation();
      /**
       * 移除小窗口上面的样式，让它隐藏
       * 1. 固定小窗口的样式  config.constant.FIXED_LI_TAG_CLASSNAME
       * 2. 由li标签控制展示的样式 config.constant.DISPLAY_LI_TAG_CLASSNAME
       * 3. 由小窗口控制展示的样式 config.constant.OTHER_DISPLAY_LI_TAG_CLASSNAME
       */
      let $parent = PROXY.getTipsContainer.call(this);
      $parent
        .removeClass(config.constant.FIXED_LI_TAG_CLASSNAME)
        .removeClass(config.constant.DISPLAY_LI_TAG_CLASSNAME)
        .removeClass(config.constant.OTHER_DISPLAY_LI_TAG_CLASSNAME);
      // 新增调用方法将hoverPool重置
      // windowsProxy.removeGroup.call(this);
      /**
       * 此时要去将可能的定时器关闭，
       * 这个时候点击完毕，小窗口就会隐藏，这个时候在小窗口上面预览的事件需要被关闭
       * 还是用定时任务去关闭吧。
       */
      util.debounce(windowsProxy.removeGroup, "appendHover", {
        args: [e],
        time: 200,
        context: this,
      });
    },

    /**
     * @method 点击选项置顶页面
     * @desc
     *
     *    点击选项，如果选项下面只有一个页面就直接置顶这个页面
     *            如果不是就展示这个分组的小窗口,并添加固定样式
     *
     *    这个固定样式在body点击事件里面，livelyPool被修改时 需要移除
     *
     *
     * @param {*} e
     */
    setGroupTop(e) {
      /**
       * 1. 获取当前预览小窗口指代的窗口描述id，要使用这个id能确认当前需要预览的窗口到底是哪一个
       */
      let id = e.target.getAttribute("lay-id");
      // 没有获取到id说明指到了别的地方需要排除
      if (id === undefined || id === null) return;
      // 判断选项代表的分组下面是不是只有一个窗口
      let flag = false;
      _lodash.every(this.livelyPool, function (v) {
        if (v.id != id) return true;
        flag = v.children.length == 1;
        if (flag) id = v.children[0];
        return false;
      });

      if (flag) {
        /**
         * 这个分组下面就只有这一个窗口，那么分组id就等于窗口id
         * 通过窗口id去判断窗口是否最小化，最小化恢复，不是就将它最小化
         */
        if (util.getWindowsMap(id).min === true) {
          // util.getPageDescribe(id).index &&
          //   layui.layer.restore(util.getPageDescribe(id).index);
          // 调用方法置顶
          this.setTop(id);
        } else {
          if (config.constant.TOP_INDEX == id) {
            util.getPageDescribe(id).index &&
              util.getWindowsMap(id).min === false &&
              layui.layer.min(util.getPageDescribe(id).index);
          } else {
            windowsProxy.setTop.call(this, id);
          }
        }
      } else {
        // 固定它下面的小窗口，此时小窗口的展示状态的，只需要添加fixed样式即可
        // 阻止事件冒泡
        if (window.event) window.event.stopPropagation();
        let $parent = PROXY.getTipsContainer.call(this);
        if ($parent.hasClass(config.constant.FIXED_LI_TAG_CLASSNAME)) {
          // 已存在说明需要隐藏
          $parent
            .removeClass(config.constant.FIXED_LI_TAG_CLASSNAME)
            .removeClass(config.constant.DISPLAY_LI_TAG_CLASSNAME)
            .removeClass(config.constant.OTHER_DISPLAY_LI_TAG_CLASSNAME);
        } else {
          $parent.addClass(config.constant.FIXED_LI_TAG_CLASSNAME);
        }
      }
    },

    /**
     * @method 添加窗口预览
     *
     * @desc
     *
     *    当鼠标移动到一个预览的小窗口上面的时候，预览鼠标所在的小窗口所对应的大窗口
     *    经过测试，需要提前保持样式不间断，否则窗口会隐藏起来
     *
     */
    appendHover(e) {
      /**
       * 1. 获取当前预览小窗口指代的窗口描述id，要使用这个id能确认当前需要预览的窗口到底是哪一个
       */
      let id = e.target.getAttribute("wid");
      // 没有获取到id说明指到了别的地方需要排除
      if (id === undefined || id === null) return;
      /**
       * 在点击关闭的之后，这个小窗口对应的窗口已经被关闭，不能再响应了，
       * 所以这里需要判断一下这个窗口的临时属性是否存在，这样可以判断它当前是否是可以响应预览操作的
       */
      if (!util.getWindowsMap(id)) return;
      /**
       * 2. 判断当前选中的窗口是否是已经在预览的窗口
       *    就是将这个获取到的id与上一次预览时保存的id进行对比，如果是一样的就不用重复处理了
       */
      if (id === this.hover.id) {
        // 获取操作栏上面的小窗口集合的dom元素
        let $target = PROXY.getTipsContainer.call(this);
        if (!$target.hasClass(config.constant.OTHER_DISPLAY_LI_TAG_CLASSNAME))
          $target.addClass(config.constant.OTHER_DISPLAY_LI_TAG_CLASSNAME);
        return;
      }
      /**
       * 将当前窗口的层级上升为顶层加1
       * 由于预览时需要将窗口出现在所有窗口的上面，所以将它放在置顶窗口的上面即可
       * 具体的做法就是将它当前的层级修改成最顶层的层级加一
       */
      util.tabsBody(id) &&
        util.tabsBody(id).css("z-index", config.constant.MAX_INDEX + 1);
      let windowDesc = util.getWindowsMap(id);
      if (windowDesc.min === true) {
        /**
         * 如果待预览的窗口当前处于最小化的状态，这里需要特殊处理一下，先临时的将它恢复
         */
        layui.layer.restore(util.getPageDescribe(id).index);
        // 添加一个临时变量，表示它是在这种特殊情况下恢复最小化的
        windowDesc.hoverMin = true;
      }
      // 设置此id为正在被预览的id
      this.hover = { id: id };
    },

    /**
     * @method 关闭窗口预览
     * @desc
     *
     *  鼠标在离开预览小窗口上面时候需要结束预览窗口
     * 这个关闭事件是针对上面的 {@linkplain windowsProxy.appendHover 小窗口预览事件}
     * 所以这里移除的样式也就是 {@linkplain config.constant.OTHER_DISPLAY_LI_TAG_CLASSNAME select-tip-flag }
     *
     *  清除样式单独处理了。
     *
     * 与上面的 {@linkplain windowsProxy.hideWindowTip 关闭小窗口} 不同，这里不仅仅是需要隐藏小窗口
     * 还需要恢复预览的窗口信息
     */
    removeHover(e) {
      /**
       * 获取当前预览小窗口指代的窗口描述id，要使用这个id能确认当前需要预览的窗口到底是哪一个
       */
      let id = e.target.getAttribute("wid");
      // 没有获取到id说明指到了别的地方需要排除
      if (id === undefined || id === null) return;
      /**
       * 在点击关闭的之后，这个小窗口对应的窗口已经被关闭，不能再响应了，
       * 所以这里需要判断一下这个窗口的临时属性是否存在，这样可以判断它当前是否是可以响应预览操作的
       */
      if (!util.getWindowsMap(id)) {
        // 移除正在预览的参数配置
        this.hover = { id: 0 };
        return;
      }
      /**
       * 这里再次确认当前操作的窗口是预览窗口
       * 这样做可以避免延时操作带来的影响
       * 测试，在点击关闭的时候这个this.hover.id总是0，所以这里就不加判断了
       */
      // if (this.hover.id !== id) return;
      // 获取窗口配置对象
      let wConfig = util.getPageDescribe(id);
      // 将层级恢复到之前的层级
      util.tabsBody(id) && util.tabsBody(id).css("z-index", wConfig.nowIndex);
      // 移除预览状态
      util.tabsBody(id) &&
        util.tabsBody(id).removeClass("layui-windows-preview");
      // 如果之前被标记为最小化就恢复最小化
      if (
        util.getWindowsMap(id).hoverMin === true &&
        util.getWindowsMap(id).min === false
      ) {
        layui.layer.min(wConfig.index);
      }
      // 清空这项配置
      util.getWindowsMap(id).hoverMin = null;
      // 移除正在预览的参数配置
      this.hover = { id: 0 };
    },

    /**
     * @method 在小窗口彻底隐藏之后，将hoverPool清空
     * @desc
     *
     *    这个方式可以在用户彻底关闭小窗口的时候清空hoverPool。
     * 然后可以在hoverPool里面判断，如果是length等于0 就不执行动画。
     * 这个是优化动画，在第一次出现小窗口的时候阻止动画，直接展示小窗口
     *    在小窗口可能关闭的几次都作出判断。在短暂的延时之后判断小窗口上面如果没有
     * 出现那三个样式之中的一个就清空hoverPool.
     *    延时就使用debounce方式来吧
     *    可能涉及到
     *  {@linkplain windowsProxy.hideWindowTip 鼠标移开li标签时}
     *  {@linkplain windowsProxy.removeHover 鼠标移开小窗口时}
     *  {@linkplain windowsProxy.setHoverTop 点击小窗口置顶时}
     *  {@linkplain windowsProxy.closeFromTip 点击小窗口关闭时 }
     *
     * 最后这个因为介入了消失动画，所以要将延时时间调大一点，并且关键字也要换成关闭时的
     */
    removeGroup() {
      let $target = PROXY.getTipsContainer.call(this);
      const condition =
        !$target.hasClass(config.constant.FIXED_LI_TAG_CLASSNAME) &&
        !$target.hasClass(config.constant.OTHER_DISPLAY_LI_TAG_CLASSNAME) &&
        !$target.hasClass(config.constant.DISPLAY_LI_TAG_CLASSNAME);
      if (condition) {
        this.hoverPool = [];
        this.hoverGroupSize = 0;
        // 新增，需要重置这个临时的去重参数
        config.temp.hoverGroupID = 0;
      }
    },

    /**
     * 取消小窗口的固定状态
     */
    disableFixedWindow() {
      // 获取操作栏上面的小窗口集合的dom元素
      let $target = PROXY.getTipsContainer.call(this);
      // 固定了就取消样式，并重置参数
      if ($target.hasClass(config.constant.FIXED_LI_TAG_CLASSNAME)) {
        $target.removeClass(config.constant.FIXED_LI_TAG_CLASSNAME);
        windowsProxy.removeGroup.call(this);
      }
    },

    /**
     * @method 将当前二级菜单加入主菜单配置
     * @param {*} wConfig
     * @desc
     *
     *    在主菜单配置初始化之时或者新的菜单配置项加入配置描述对象之前(前提是指定了pid)
     * 将这个菜单的配置项传递进来,通过配置项的pid将这个配置放入主菜单配置的响应位置
     */
    joinInMenu(wConfig) {
      let findFlag = true;
      _lodash.every(config.base.menus, (menu) => {
        _lodash.every(menu.children, (group) => {
          if (group.id === wConfig.pid) {
            group.children.push({
              id: wConfig.id,
              name: wConfig.name,
              type: "page",
              icon: wConfig.icon || "layui-icon-file",
              iconColor:
                wConfig.iconColor ||
                "rgba(var(--windows-main-button-color), 1)",
            });
            findFlag = false;
          }
          return findFlag;
        });
        return findFlag;
      });
    },
  };

  /**
   * 窗口事件集合
   */
  let windowsEvents = {
    /**
     * @method 执行正在摇晃窗口时的动作
     * @param {*} e
     */
    onShaking(e) {
      // 1. 首先确定是真的有窗口被捕获
      if (!config.temp.disposeWindow.id) return;
      let [X, Y] = [e.clientX, e.clientY];
      // 2. 初始化缓存对象
      if (!config.temp.disposeWindow.shake) {
        // 第一次初始化
        config.temp.disposeWindow.shake = {
          X: X,
          Y: Y,
          AWAKE: true,
          ACTION: [],
        };
        return;
      }
      if (config.temp.disposeWindow.shake.AWAKE === false) return;
      // 3. 判断方向
      let point = 0;
      if (
        Math.abs(config.temp.disposeWindow.shake.X - X) >
          config.constant.BASE_WINDOWS_OFFSET &&
        Math.abs(config.temp.disposeWindow.shake.Y - Y) <=
          config.constant.BASE_WINDOWS_OFFSET
      ) {
        // 判断为左右 往左为1 往右为9
        point = config.temp.disposeWindow.shake.X > X ? 1 : 9;
      }
      if (
        Math.abs(config.temp.disposeWindow.shake.Y - Y) >
          config.constant.BASE_WINDOWS_OFFSET &&
        Math.abs(config.temp.disposeWindow.shake.X - X) <=
          config.constant.BASE_WINDOWS_OFFSET &&
        point == 0
      ) {
        // 判断为上下 往上为2 往下为8
        point = config.temp.disposeWindow.shake.Y > Y ? 2 : 8;
      }
      // 4. 将判断结果放入栈中
      if (config.temp.disposeWindow.shake.ACTION.length == 0 && point !== 0) {
        config.temp.disposeWindow.shake.ACTION.push(point);
        config.temp.disposeWindow.shake.X = X;
        config.temp.disposeWindow.shake.Y = Y;
        return;
      } else {
        if (
          point +
            config.temp.disposeWindow.shake.ACTION[
              config.temp.disposeWindow.shake.ACTION.length - 1
            ] ==
          10
        ) {
          config.temp.disposeWindow.shake.ACTION.push(point);
          config.temp.disposeWindow.shake.X = X;
          config.temp.disposeWindow.shake.Y = Y;
        }
      }
      // 5. 判断是否重复了4次
      if (config.temp.disposeWindow.shake.ACTION.length >= 4) {
        // 执行min方法
        _lodash.each(util.getWindowsMap(), (v, k) => {
          if (v.id !== config.temp.disposeWindow.id && v.min === false) {
            util.getPageDescribe(k) &&
              util.getPageDescribe(k).index &&
              layui.layer.min(util.getPageDescribe(k).index);
          }
        });
        // 修改判断标志
        config.temp.disposeWindow.shake.AWAKE = false;
      }
    },

    /**
     * @method 执行窗口结束操作-摇晃操作结束
     * @param {*} e
     */
    onShaken(e) {
      // 1. 首先确定是真的有窗口被捕获
      if (!config.temp.disposeWindow.id) return;
      // 2. 移除临时变量
      delete config.temp.disposeWindow.shake;
    },

    /**
     * @method 执行正在选择分屏操作的动作
     * @param {*} e
     */
    onDividing(e) {
      // 1. 首先确定是真的有窗口被捕获
      if (!config.temp.disposeWindow.id) return;
      // 2. 如果是有分隔的参数就恢复
      let _id = config.temp.disposeWindow.id;
      if (util.getWindowsMap(_id).dividedWidth) {
        let layero = util.getPageDescribe(_id).parent;
        layero.css({
          width: util.getWindowsMap(_id).dividedWidth + "px",
          height: util.getWindowsMap(_id).dividedHeight + "px",
        });
        util.getWindowsMap(_id).dividedWidth = 0;
        util.getWindowsMap(_id).dividedHeight = 0;
      }
      // 3. 判断鼠标是否已经进入规定的区域
      if (e.clientY >= config.constant.BASE_WINDOWS_TOP_OFFSET) {
        // 没有就返回
        config.base.divide.ACTION = 0;
        config.base.divide.point = 0;
        return;
      }
      // 4. 修改配置展示操作条
      config.base.divide.ACTION = 1;
      // 5. 依次判断模式
      document.querySelectorAll(".layui-windows-divide").forEach((divide) => {
        let _left = divide.getBoundingClientRect().left;
        let _width = divide.getBoundingClientRect().width;
        if (e.clientX >= _left && e.clientX - _left <= _width) {
          $(divide)
            .removeClass("layui-windows-divide-hover")
            .addClass("layui-windows-divide-hover");
          config.base.divide.point = divide.getAttribute("dividekey");
        } else {
          $(divide).removeClass("layui-windows-divide-hover");
        }
      });
    },

    /**
     * @method 执行选择分屏结束操作的动作
     * @param {*} e
     */
    onDivided(e) {
      // 1. 首先确定是真的有窗口被捕获
      if (!config.temp.disposeWindow.id) return;
      // 2. 判断操作栏是否打开
      if (config.base.divide.ACTION == 0 || config.base.divide.point == 0) {
        // 没有就返回
        config.base.divide.ACTION = 0;
        config.base.divide.point = 0;
        return;
      }
      // 3. 根据point参数调整窗口的位置和大小(还有要保存之前的状态)
      // 获取当前窗口的高宽
      let id = config.temp.disposeWindow.id;
      let layero = util.getPageDescribe(id).parent;
      let clientHeight = parseInt(layero.css("height"));
      let clientWidth = parseInt(layero.css("width"));
      util.getWindowsMap(id).dividedWidth = clientWidth;
      util.getWindowsMap(id).dividedHeight = clientHeight;
      // 计算当前的高宽，top和left
      let bodyHeight =
        parseInt($body.css("height")) -
        config.constant.BASE_WINDOWS_TOP_OFFSET -
        config.constant.BASE_WINDOWS_BOTTOM_OFFSET;
      let bodyWidth =
        parseInt($body.css("width")) -
        config.constant.BASE_WINDOWS_LEFT_OFFSET -
        config.constant.BASE_WINDOWS_RIGHT_OFFSET;
      // 获取配置
      let _config = DEFAULT_DIVIDE[config.base.divide.point];
      let width = bodyWidth * _config.width;
      let height = bodyHeight * _config.height;
      let top =
        _config.top == 0
          ? config.constant.BASE_WINDOWS_TOP_OFFSET
          : bodyHeight - height + config.constant.BASE_WINDOWS_TOP_OFFSET;
      let left =
        _config.left == 0
          ? config.constant.BASE_WINDOWS_LEFT_OFFSET
          : bodyWidth - width + config.constant.BASE_WINDOWS_LEFT_OFFSET;
      // 修改窗口样式
      layero.css({
        width: width + "px",
        height: height + "px",
        top: top + "px",
        left: left + "px",
      });
      // 4. 重置参数
      config.base.divide.ACTION = 0;
      config.base.divide.point = 0;
    },
  };

  /**
   * 定义整个菜单控制模块
   */
  layui.binder.extend({
    name: "windows",
    data: config.base,
    template: `
    <div>
      <div class="layui-windows-desk" :class = "layui-windows-desk-{{menusVisable}}">
        <div class = "layui-windows-user-container">
          <div class = "layui-windows-user-pane">
            <div title = "设置" ><i class = "layui-icon layui-icon-set-sm"></i>设置</div>
            <div title = "退出" ><i class = "layui-icon layui-icon-logout"></i>退出</div>
          </div>
        </div>
        <div class = "layui-windows-menu-container">
          <div class = "layui-windows-menu-scroll layui-windows-desk-scroll">
            <div class = "layui-windows-menu-body layui-windows-desk-scroll" :class = "layui-windows-menu-body-{{menusTrigger}}"  v-each = "menus"  mark = "menu" >
              <div v-if = "menu.children.length !== 0"> 
                <div class = "layui-windows-menu-char" :lay-menuid = "menu.id"  >{{menu.name}}</div>
                <div v-each = "menu.children" mark = "group" >
                  <div :class = "layui-windows-menu-group-{{group.spread}}">
                    <div class = "layui-windows-menu-group" :lay-menuid = "group.id" >
                      <i class="layui-icon layui-menu-icon-group" :class = "group.icon" :style = "color:{{group.iconColor}}"></i>
                      {{group.name}}
                      <i class="layui-icon layui-menu-icon-arrows" :class = "group.spread ? 'layui-icon-up' : 'layui-icon-down'"></i>
                    </div>
                    <div class = "layui-windows-menu-pages">
                      <div v-each = "group.children" mark = "page">
                        <div class = "layui-windows-menu-page" :lay-menuid = "page.id">
                          <i class="layui-icon layui-menu-icon-page" :class = "page.icon" :style = "color:{{page.iconColor}}"></i>
                          {{page.name}}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class = "layui-windows-menu-list layui-windows-desk-scroll" :class = "layui-windows-menu-list-{{menusTrigger}}" v-each = "menus"  mark = "menu">
              <span :class = "menu.children.length !== 0 ? 'layui-windows-span-able' : 'layui-windows-span-disable'"  >{{menu.name}}</span>
            </div>
          </div>
        </div>
      </div>
      <div class="layui-windows-divides" :class = "divideClass" v-each = "divide.source" mark = "divide"  >
        <div class="layui-windows-divide" :title = "divide.title" :dividekey = "divide.key"  >
          <div class = "layui-windows-divide-i"></div>
        </div>
      </div>
      <div class="layui-windows-dividing" v-if = "divide.ACTION == 1" ></div>
      <div id="${config.constant.CONTAINER_ID}" class = "${config.constant.MAX_CLASSNAME}">
        <div class="layui-windows-menu-icon" title = "打开菜单" @click = "toggleMenu" >
          <i class="layui-icon layui-icon-release"></i>
        </div>
        <div class="layui-windows-group">
          <div class="layui-icon layadmin-tabs-control layui-icon-prev" @click="rollPageLeft"></div>
          <div class="layui-icon layadmin-tabs-control layui-icon-next" @click="rollPageRight"></div>
          <div class="layui-tab">
            <ul class="layui-tab-title" id="${config.constant.HEADERS_ID}" v-each="livelyPool" mark="arr" key = "index" >
              <li :class="layui-tab-title-{{arr.select}}" :lay-id="arr.id"  @mouseenter="showWindowTip"  @mouseleave="hideWindowTip" @mousemove = "showWindowTip"  @click="setGroupTop" >
                <i class="layui-icon" :class = "arr.icon" :style = "color:{{arr.iconColor}}"></i> {{arr.name}}
              </li>
            </ul>
          </div>
        </div>
      <!--  <div class="layui-windows-notice" title = "关闭全部" @click = "minAll" >
          <i class="layui-icon layui-icon-close"></i>
        </div>  -->
        <div class="${config.constant.CONTAINER_TIPS_CLASSNAME}" :class = "hoverClass"  @mouseenter="hoverTip"  @mouseleave="hideWindowTip" v-each="hoverPool" mark="arr" key = "id"  lay-event = "updateTipSize"  >
          <div class = "layui-windows-tips"  @mouseenter="appendHover" @mouseleave="removeHover" @mousemove = "appendHover"  :wid = "arr.id" @click="setHoverTop">
            <i class="layui-icon layui-icon-close" @click="closeFromTip" :wid = "arr.id" title = "关闭" ></i>
            <div :wid = "arr.id">{{arr.name}}</div>
          </div>  
        </div>
      </div>
    </div>  
    `,

    beforeCreate() {
      /**
       * @since v4.2.0
       * @desc
       *    在数据落地之前，先将窗口配置项里面的内容写入
       * 主菜单配置项里面，在比起后面在进行数据初始化完毕之后在处理可以少进行一些渲染步骤
       */
      // 放入第一层的首字母
      _lodash.each(CHARACTER_MAP.split("、"), (c) => {
        config.base.menus.push({
          id: "character" + c,
          name: c,
          reg: new RegExp("^" + c),
          type: "character",
          children: [],
        });
      });
      // 放入 # 号键
      config.base.menus.push({
        id: "character#",
        name: "#",
        reg: /^[^A-z]/,
        type: "character",
        children: [],
      });
      /**
       * 获取配置项里面的一级菜单配置项  带有 mark 属性的
       * 这个一级菜单配置项后面添加的就直接忽略，只计算这里能扫面到的一级菜单
       */
      _lodash.each(config.pageDescribe, (page) => {
        if (page.mark) {
          _lodash.every(config.base.menus, (character) => {
            if (character.reg.test(String(page.mark).toLocaleUpperCase())) {
              character.children.push({
                id: page.id,
                name: page.name,
                type: "group",
                spread: false,
                icon: page.icon || "layui-icon-export",
                iconColor:
                  page.iconColor || "rgba(var(--windows-main-layui-color), 1)",
                children: [],
              });
              return false;
            }
            return true;
          });
        }
      });
      /**
       * 接下来是二级菜单的初始化，因为后面动态处理的时候
       * 也可能新增二级菜单，所以将这个方法放出去
       */
      _lodash.each(config.pageDescribe, (page) => {
        if (!page.mark && page.pid) windowsProxy.joinInMenu(page);
      });
    },

    watchs: {
      hover(value) {
        if (value.id) {
          // 只查看id这个窗口
          util
            .tabsBody(value.id)
            .removeClass("layui-windows-preview")
            .addClass("layui-windows-preview");
          $("body")
            .removeClass("layui-windows-preview")
            .addClass("layui-windows-preview");
        } else {
          // 退出预览模式
          $(".layui-windows-preview").removeClass("layui-windows-preview");
        }
      },
      livelyPool() {
        // $("." + config.constant.FIXED_LI_TAG_CLASSNAME).removeClass(
        //   config.constant.FIXED_LI_TAG_CLASSNAME
        // );
        windowsProxy.disableFixedWindow.call(this);
      },

      /**
       * 由于在触发这个函数时不能保证小窗口的dom更新已经完毕
       * 这个里面对dom的操作会失效，所以将这部分的内容转移到method中
       * 通过each指令的回调进行调用.
       *
       * 这里的新功能是修改li标签的样式
       * 在切换这个数组时说明选择预览的分组发生了改变，这个时候需要查找到当前正在预览的分组
       * 将它对应的li标签添加上layui-tab-title-hover样式，保证它的hover样式触发
       */
      hoverPool(value, oldValue) {
        // if (value && value instanceof Array && value.length > 0) {
        /**
         * 首先获取到第一个可以预览的li标签
         * 通过循环遍历在线池里面的在线列表，这个列表至少有一个子窗口的说明是可以预览的
         * 直接查看它的children的长度。
         *
         * 暂时不用判断了
         */
        // let firstId = null;
        // _lodash.every(this.$data.hoverPool, (p) => {
        //   if (p.children == 0) return true;
        //   firstId = p.groupid;
        //   return false;
        // });
        // // 没有找到首个符合条件的,或者这个dom元素不存在的就返回
        // if (!firstId) return;
        // let $firstDom = $("#" + config.constant.HEADERS_ID).find(
        //   '[lay-id="' + firstId + '"]'
        // );
        // if (!$firstDom.get(0)) return;
        // }
        $(".layui-tab-title-hover").removeClass("layui-tab-title-hover");
        if (value && value instanceof Array && value.length > 0) {
          /**
           * 由鼠标移动监听事件 {@linkplain windowsProxy.showWindowTip}
           * 知当前预览的分组id是 {@linkplain config.temp.hoverGroupID}
           */
          let $dom = $("#" + config.constant.HEADERS_ID).find(
            '[lay-id="' + config.temp.hoverGroupID + '"]'
          );
          $dom.addClass("layui-tab-title-hover");
        }
      },
    },

    computeds: {
      hoverClass() {
        return this.hoverPool.length == 0 ? config.constant.HIDE : "";
      },
      divideClass() {
        return this.divide.ACTION == 1 ? "" : config.constant.HIDE;
      },
    },

    methods: {
      /**
       * @method 小窗口dom更新回调函数
       *    在小窗口更新时可能会有窗口大小的变化
       * 这里使用jQuery的动画来对小窗口的dom添加动画操作
       *
       * @param {*} ele       指令回调，指代的dom
       * @param {*} binding   指令回调参数
       */
      updateTipSize(ele, binding) {
        let value = binding.value;
        if (value && value instanceof Array && value.length > 0) {
          /**
           * 获取第一个li标签，后面需要用到它的位置信息
           *  上面的那个id就不一定是第一个了，要考虑到在操作栏上面固定的li
           * 检查这第一个li标签的dom是否存在
           */
          let $this = $("#" + config.constant.HEADERS_ID).find(
            '[lay-id="' + this.$data.hoverPool[0].groupid + '"]'
          );
          if (!$this.get(0)) return;
          // 获取小窗口jq对象，方便后面的动画操作
          let $target = $("." + config.constant.CONTAINER_TIPS_CLASSNAME);
          // 获取当前dom元素的位置
          let left = $this.get(0).getBoundingClientRect().left;
          // 获取到第一个li标签的宽度，因为作用的li标签都设置成了一个宽度，这个可以代表所有的li标签的宽度
          let width = $this.outerWidth();
          /**
           * 所以最终的left位置
           *    hoverGroupWidth ...   hoverGroupWidth
           *               left + width
           * 就是比当前的left还要left小窗口的最终宽度减li标签本身的宽度 整体的一半
           */
          let finalLeft =
            left - (this.hoverGroupWidth * value.length - width) / 2;
          /**
           * 如果上一次的值是一个空数组，这一次的值不是一个空数组的话
           * 直接将位置放过去即可返回
           */
          if (this.hoverGroupSize == 0) {
            // 设置小窗口位置,这个宽度还是要重新设置的
            $target.css({
              left: finalLeft < 0 ? 0 : finalLeft + "px",
              width: this.hoverGroupWidth * value.length + "px",
            });
            // 记录下当前小窗口中子窗口的数量
            this.hoverGroupSize = value.length;
            return;
          }
          /**
           * 下面执行小窗口动画部分：
           *    ***  这个方法再设置小窗口位置时会同时将此时子窗口的数量记录在 hoverGroupSize 中
           * 1. 如果记录的 hoverGroupSize 数量和当前的value.length 相同，那么小窗口的大小是不用变化的，只需要将位置动画到指定的位置即可
           * 2. 如果两者的数量不同，说明小窗口的大小需要发生变化
           *    如果数量减少  hoverGroupSize > value.length
           *        那么动画应该从大到小即可。直接将小窗口的宽度也加入变化即可
           *    如果数量增大
           *        那么动画应该从小到大。
           *
           */
          if (this.hoverGroupSize && this.hoverGroupSize != value.length) {
            /**
             * 同上，这次计算的是按照上一次的子窗口个数。它的left位置应该处于哪里
             */
            let _finalLeft =
              left - (this.hoverGroupWidth * this.hoverGroupSize - width) / 2;
            if (this.hoverGroupSize > value.length) {
              /**
               * 先将小窗口的宽度和位置调整成上一次的。方便后面的动画
               */
              $target.css({
                width: this.hoverGroupWidth * this.hoverGroupSize + "px",
                left: _finalLeft < 0 ? 0 : _finalLeft + "px",
              });
              /**
               * 执行动画。
               * 将 宽度缩小 将 left进行调整
               */
              $target.animate(
                {
                  left: finalLeft < 0 ? 0 : finalLeft + "px",
                  width: this.hoverGroupWidth * value.length + "px",
                },
                200
              );
            } else {
              // 记录被改变的dom
              // let tempDoms = [];
              // 记录操作下标，识别出新增的下标
              // let tempIndex = this.hoverGroupSize;
              let scale = this.hoverGroupSize / value.length;
              /**
               * 首先，将其它的。新增的子窗口修改样式，然后将它们放入一个队列里面
               *
               * 改成全部的都改样式
               * 上面的方法，动画有点僵硬的感觉。会感觉抖动厉害
               */
              $target.find(".layui-windows-tips").each(function () {
                // if (tempIndex <= 0) {
                // tempDoms.push($(this));
                $(this).css("transition", "none");
                $(this).css("transform", "scale(" + scale + ")");
                // }
                // tempIndex--;
              });
              /**
               * 和上面的一样，修改小窗口的宽度和位置
               */
              $target.css({
                width: this.hoverGroupWidth * this.hoverGroupSize + "px",
                left: _finalLeft < 0 ? 0 : _finalLeft + "px",
              });
              /**
               * 执行动画。
               * 将 宽度增大 将 left进行调整
               */
              $target.animate(
                {
                  left: finalLeft < 0 ? 0 : finalLeft + "px",
                  width: this.hoverGroupWidth * value.length + "px",
                },
                200
              );
              /**
               * 执行动画。
               */
              // _lodash.each(tempDoms, (dom) => {
              //   dom.css("transition", "all .3s");
              //   dom.css("transform", "scale(1)");
              // });
              $target.find(".layui-windows-tips").each(function () {
                $(this).css("transition", "all .3s");
                $(this).css("transform", "scale(1)");
              });
            }
          } else {
            $target.animate(
              {
                left: finalLeft < 0 ? 0 : finalLeft + "px",
              },
              200
            );
          }
          this.hoverGroupSize = value.length;
        }
      },
      /**
       * 将操作栏上面的li标签整体向左移动
       *
       *        主要是在点击操作栏上面的对应小工具按钮时触发
       */
      rollPageLeft() {
        rollPage.left(PROXY.getActionContainer.call(this));
      },
      /**
       * 将操作栏上面的li标签整体向右移动
       *
       *        主要是在点击操作栏上面的对应小工具按钮时触发
       */
      rollPageRight() {
        rollPage.right(PROXY.getActionContainer.call(this));
      },
      /**
       * 调整操作栏，使对应的li标签露出
       *
       *        在setTop等操作后自适应下面操作栏
       *
       * @param {*} index  窗口的id(pageDescribe配置的key)
       */
      rollPageAuto(index) {
        rollPage.auto(PROXY.getActionContainer.call(this), index);
      },

      open(opt, data) {
        windowsProxy.open.call(this, opt, data);
      },

      /**
       * @method 将指定的窗口置顶
       * @param {String} id 这个指在pageDescribe配置项里面的key或者id
       */
      setTop(id = config.constant.TOP_INDEX) {
        windowsProxy.setTop.call(this, id);
      },

      resize(id, fn) {
        windowsProxy.resize.call(this, id, fn);
      },

      /**
       * @method 展示小窗口
       * @desc
       *
       *    选项dom移上去的监听事件
       *
       *    在鼠标移至操作栏上面的选项卡分组时，弹出分组下面的预览小窗口
       *
       * @todo 后面可以缓存这个当前分组的id在相同的情况下就不用重复渲染dom了
       *
       * @param {*} e
       */
      showWindowTip(e) {
        // e.stopPropagation();
        // e.preventDefault();
        util.debounce(windowsProxy.showWindowTip, "showWindowTip", {
          args: [e],
          time: 30,
          context: this,
        });
      },

      /**
       * @method 关闭小窗口
       * @desc
       *
       *    操作栏dom移开时或者小窗口移开时的监听事件
       *    如果是从操作栏到小窗口的移动或者是小窗口到操作栏的移动都是没得问题的 {@linkplain windowsProxy.hoverTip 保持小窗口}
       *
       *
       *    1.当鼠标离开时会将分组选项卡里面的 select-tip-flag 样式移除，但是如果它跳到了 小窗户 上面，会在上面hoverTip方法里面
       *     将样式重新加回来。
       *    2.如果鼠标短暂的离开分组选项卡和小窗口  select-tip-flag 样式移除 因为受其它的影响，暂时还不会关闭小窗口
       *    3.如果超过一定时间没有进入其它方法将 select-tip-flag 样式补回来，就认为是真的离开了，执行关闭小窗口的方法
       *
       */
      hideWindowTip() {
        // util.debounce(windowsProxy.hideWindowTip, { time: 100, context: this });
        // windowsProxy.hideWindowTip.call(this);
        util.debounce(windowsProxy.hideWindowTip, "showWindowTip", {
          time: 30,
          context: this,
        });
      },

      /**
       * @method 点击选项置顶页面
       * @desc
       *
       *    点击选项，如果选项下面只有一个页面就直接置顶这个页面
       *            如果不是就展示这个分组的小窗口,并添加固定样式
       *
       *    这个固定样式在body点击事件里面，livelyPool被修改时 需要移除
       *
       *
       * @param {*} e
       */
      setGroupTop(e) {
        windowsProxy.setGroupTop.call(this, e);
      },

      /**
       * @method 添加窗口预览
       *
       * @desc
       *    预览小窗口鼠标移上去的事件
       *
       *    预览鼠标所在的小窗口所对应的大窗口
       *
       */
      appendHover(e) {
        /**
         * 需要提前的来保证样式存在，否则会出现小窗口隐藏的现象
         */
        let $target = PROXY.getTipsContainer.call(this);
        if (!$target.hasClass(config.constant.OTHER_DISPLAY_LI_TAG_CLASSNAME))
          $target.addClass(config.constant.OTHER_DISPLAY_LI_TAG_CLASSNAME);

        util.debounce(windowsProxy.appendHover, "appendHover", {
          args: [e],
          time: 200,
          context: this,
        });
        // windowsProxy.appendHover.call(this, e);
      },

      /**
       * @method 关闭窗口预览
       * @desc
       *
       *  鼠标在离开预览小窗口上面时候需要结束
       *
       *  首先这个隐藏小窗口动作放入 debounce 函数中，它可以和 上面的appendHover 方法衔接
       *
       * 但是这个结束的方法 removeHover 是一定，必须要恢复的。
       *
       *
       */
      removeHover(e) {
        let $target = PROXY.getTipsContainer.call(this);
        util.debounce(
          function () {
            /**
             * 隐藏小窗口
             */
            $target.removeClass(config.constant.OTHER_DISPLAY_LI_TAG_CLASSNAME);

            /**
             * 新增调用方法将hoverPool重置
             *
             * 这里调整一下顺序，无论这两个id是否一致都去检查下是否应该重置数据
             * 这一步就放在取消了这个样式之后
             *
             */
            windowsProxy.removeGroup.call(this);
          },
          "appendHover",
          {
            time: 400,
            args: [e],
            context: this,
          }
        );
        windowsProxy.removeHover.call(this, e);
      },

      /**
       * @method 点击预览小窗口置顶它对应的窗口
       */
      setHoverTop(e) {
        windowsProxy.setHoverTop.call(this, e);
      },

      /**
       * @method 通过小窗口关闭页面
       * @desc
       *
       *    预览小窗口的右上方有一个红叉，点击这里可以关闭对应的窗口
       *
       *    关闭之后需要隐藏小窗口:
       *      因为关闭窗口之后会去修改windowsMap和livelyPool,
       *      再次触发检索的时候就会重新的去获取hoverPool列表
       *      这种方式才能更新预览列表
       *
       * @todu 如果设置了hover组id的缓存，这里需要删除这个缓存，因为这个执行之后需要强制去更新hoverPool
       * @param {*} e
       */
      closeFromTip(e) {
        windowsProxy.closeFromTip.call(this, e);
      },

      closeAll() {
        windowsProxy.closeAll();
      },

      toggleMenu(e) {
        e.stopPropagation();
        e.preventDefault();
        if (this.menusVisable == "true") {
          this.menusVisable = "false";
        } else {
          this.menusVisable = "true";
        }
      },
    },
    mounted() {
      let self = this;

      /**
       * 切换分割类型的时候修改下面模拟展示窗口的样式
       * 这种方式修改可以触发css3的动画效果
       */
      self.$watch("divide.point", function (v) {
        this.divide.class = v ? DEFAULT_DIVIDE[v].class : "";
        if (v) {
          if (!config.temp.disposeWindow.id) return;
          // 计算当前的高宽，top和left
          let bodyHeight =
            parseInt($body.css("height")) -
            config.constant.BASE_WINDOWS_TOP_OFFSET -
            config.constant.BASE_WINDOWS_BOTTOM_OFFSET;
          let bodyWidth =
            parseInt($body.css("width")) -
            config.constant.BASE_WINDOWS_LEFT_OFFSET -
            config.constant.BASE_WINDOWS_RIGHT_OFFSET;
          let _config = DEFAULT_DIVIDE[v];
          let width = bodyWidth * _config.width;
          let height = bodyHeight * _config.height;
          let top =
            _config.top == 0
              ? config.constant.BASE_WINDOWS_TOP_OFFSET
              : bodyHeight - height + config.constant.BASE_WINDOWS_TOP_OFFSET;
          let left =
            _config.left == 0
              ? config.constant.BASE_WINDOWS_LEFT_OFFSET
              : bodyWidth - width + config.constant.BASE_WINDOWS_LEFT_OFFSET;
          $(this.parent)
            .find(".layui-windows-dividing")
            .css({
              width: width + "px",
              height: height + "px",
              top: top + "px",
              left: left + "px",
            });
        }
      });

      if (!PROPERTY.FIRST_LOADING) {
        PROPERTY.FIRST_LOADING = true;
        $body.on("click", ".layui-windows-menu-page", function (e) {
          let id = e.target.getAttribute("lay-menuid");
          // if(id) layui.layer.open(id);
          if (id) layui.windows.open(id);
        });
        $body.on("click", ".layui-windows-menu-char", function (e) {
          e.stopPropagation();
          e.preventDefault();
          self.menusTrigger = "true";
        });

        $body.on("click", ".layui-windows-span-able", function (e) {
          e.stopPropagation();
          e.preventDefault();
          self.menusTrigger = "false";
        });

        $body.on("click", ".layui-windows-span-disable", function (e) {
          e.stopPropagation();
          e.preventDefault();
        });

        $body.on("click", ".layui-windows-menu-group", function (e) {
          e.stopPropagation();
          e.preventDefault();
          let id = $(this).attr("lay-menuid");
          let flag = true;
          _lodash.every(self.menus, (menu) => {
            _lodash.every(menu.children, (group) => {
              if (group.id == id) {
                group.spread = !group.spread;
                flag = false;
              }
              return flag;
            });
            return flag;
          });
        });

        $body.on("click", function (e) {
          // 2.取消固定的分组
          // $("." + config.constant.FIXED_LI_TAG_CLASSNAME).removeClass(
          //   config.constant.FIXED_LI_TAG_CLASSNAME
          // );
          windowsProxy.disableFixedWindow.call(self);
          self.menusVisable = "false";
        });

        $body.on("click", ".layui-windows-desk", function (e) {
          e.stopPropagation();
          e.preventDefault();
        });

        $body.on("mousemove", function (e) {
          // 1. shake 判断
          windowsEvents.onShaking(e);
          // 2. divide 判断
          windowsEvents.onDividing(e);
          // 3. 范围内的移动
        });
        $body.on("mouseup", function (e) {
          // 1. divide 判断结束
          windowsEvents.onDivided(e);
          // 2. shake 判断结束
          windowsEvents.onShaken(e);
          // 3. 释放窗口
          windowsProxy.releaseWindow();
        });
      }
    },
  });

  /**
   * 最后暴露出方法
   */
  let handler = {
    wakeUp: function (option, list) {
      return windowsProxy.wakeUp(option, list);
    },
    notify: function () {
      return windowsProxy.notify();
    },
    instance: function () {
      return VM.windows;
    },
    open: function (id) {
      return config.constant.MENU_CLICK_EVENT(id);
    },
  };

  layui.layer.wakeUp = handler.wakeUp;

  return handler;
});
