# layui_steps

#### 介绍
layui步骤条

#### 软件架构
> css 添加steps.css 用来调整步骤条的css样式
> js 添加steps.js 处理步骤条的渲染

#### 安装教程

1.  引入layui的css和js文件
2.  引入steps.css
3.  引入steps模块

#### 使用说明

1.  在页面dom上面选择一个div，在这脸添加一个className为 layui-steps-area的div,作为步骤条容器。
2.  在上面创建出的步骤条容器上面添加属性lay-mark-total，其属性值是需要渲染的步骤数量
3.  使用layui.steps.build(destination)渲染步骤条。destination指选择的最外层div的jq对象。
